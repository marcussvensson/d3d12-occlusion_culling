#pragma once

#include <d3d12.h>
#include <DirectXMath.h>

struct WorldVertex
{
    DirectX::XMFLOAT3 position;
    DirectX::XMFLOAT4 color;
};
struct WorldConstantBuffer
{
    DirectX::XMFLOAT4X4 gModel;
    DirectX::XMFLOAT4X4 gViewProjection;

    float padding[ 32 ];
};

struct ViewVertex
{
    DirectX::XMFLOAT3 position;
    DirectX::XMFLOAT2 uv;
};
struct ViewConstantBuffer
{
    DirectX::XMFLOAT4X4 gModel;
};

struct OcclusionCullingBufferData
{
    DirectX::XMFLOAT4 gFrustumPlanes[ 6 ];
    UINT gAabbCount;
};
struct AABB
{
    DirectX::XMFLOAT3 points[ 2 ];
};
struct IndirectCommand
{
    D3D12_GPU_VIRTUAL_ADDRESS cbvAddress;
    D3D12_DRAW_INDEXED_ARGUMENTS drawArguments;
};