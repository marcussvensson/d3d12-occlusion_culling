#pragma once

#include <stdio.h>
#include <cassert>
#include <Windows.h>
#include <comdef.h>

#ifdef _DEBUG
#define LOG( message, ... ) printf( message, __VA_ARGS__ )
#define HR( hr ) if ( FAILED( hr ) ) { _com_error error( hr ); LOG( "%s\n", error.ErrorMessage() ); throw; }
#else
#define LOG( message, ... )
#define HR( hr )
#endif

typedef unsigned short Index;