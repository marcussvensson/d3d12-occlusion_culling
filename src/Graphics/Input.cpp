#include "Input.h"

namespace Input
{
    // Keyboard
    std::bitset<SDL_NUM_SCANCODES> KeysDown;
    std::bitset<SDL_NUM_SCANCODES> KeysUp;
    std::bitset<SDL_NUM_SCANCODES> KeysPressed;
    std::bitset<SDL_NUM_SCANCODES> KeysReleased;

    // Mouse
    std::bitset<8> MouseButtonsDown;
    std::bitset<8> MouseButtonsUp;
    std::bitset<8> MouseButtonsPressed;
    std::bitset<8> MouseButtonsReleased;

    Sint32 MousePosition[ 2 ];
    Sint32 DeltaMousePosition[ 2 ];


    void Reset()
    {
        KeysPressed.reset();
        KeysReleased.reset();

        MouseButtonsPressed.reset();
        MouseButtonsReleased.reset();

        DeltaMousePosition[ 0 ] = 0;
        DeltaMousePosition[ 1 ] = 0;
    }

    void ProcessEvent( SDL_Event& event )
    {
        switch ( event.type )
        {
            case SDL_KEYDOWN:
                if ( !event.key.repeat )
                {
                    KeysUp[ event.key.keysym.scancode ] = false;
                    KeysDown[ event.key.keysym.scancode ] = true;

                    KeysPressed[ event.key.keysym.scancode ] = true;
                }
                break;
            case SDL_KEYUP:
                KeysDown[ event.key.keysym.scancode ] = false;
                KeysUp[ event.key.keysym.scancode ] = true;

                KeysReleased[ event.key.keysym.scancode ] = true;
                break;
            case SDL_MOUSEMOTION:
                MousePosition[ 0 ] = event.motion.x;
                MousePosition[ 1 ] = event.motion.y;
                DeltaMousePosition[ 0 ] = event.motion.xrel;
                DeltaMousePosition[ 1 ] = event.motion.yrel;
                break;
            case SDL_MOUSEBUTTONDOWN:
                MouseButtonsUp[ event.button.button ] = false;
                MouseButtonsDown[ event.button.button ] = true;

                MouseButtonsPressed[ event.button.button ] = true;
                break;
            case SDL_MOUSEBUTTONUP:
                MouseButtonsDown[ event.button.button ] = false;
                MouseButtonsUp[ event.button.button ] = true;

                MouseButtonsReleased[ event.button.button ] = true;
                break;
        }
    }

    const bool IsKeyDown( SDL_Scancode key )
    {
        return KeysDown[ key ];
    }
    const bool IsKeyUp( SDL_Scancode key )
    {
        return KeysUp[ key ];
    }
    const bool IsKeyPressed( SDL_Scancode key )
    {
        return KeysPressed[ key ];
    }
    const bool IsKeyReleased( SDL_Scancode key )
    {
        return KeysReleased[ key ];
    }

    const bool IsMouseButtonDown( Uint8 button )
    {
        return MouseButtonsDown[ button ];
    }
    const bool IsMouseButtonUp( Uint8 button )
    {
        return MouseButtonsUp[ button ];
    }
    const bool IsMouseButtonPressed( Uint8 button )
    {
        return MouseButtonsPressed[ button ];
    }
    const bool IsMouseButtonReleased( Uint8 button )
    {
        return MouseButtonsReleased[ button ];
    }

    const Sint32* GetMousePosition()
    {
        return MousePosition;
    }
    const Sint32* GetDeltaMousePosition()
    {
        return DeltaMousePosition;
    }
}