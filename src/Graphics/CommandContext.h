#pragma once

#include "ThreadContext.h"

#include <d3d12.h>

namespace Graphics
{
    class CommandContext
    {
    private:
        ID3D12CommandQueue* mCommandQueue;
        ID3D12CommandAllocator* mCommandAllocator;
        ID3D12GraphicsCommandList* mCommandList;

        ID3D12Fence* mFence;
        UINT64 mFenceValue;
        HANDLE mFenceEvent;

    public:
        void Create( D3D12_COMMAND_LIST_TYPE type, D3D12_FENCE_FLAGS fenceFlags );
        void Destroy();

        void Wait( ID3D12Fence* fence, UINT64 fenceValue );
        void Wait( CommandContext& commandContext );
        void Wait( ThreadContext& threadContext );

        void WaitForGpu();

        void ResetCommandList( ID3D12PipelineState* initialPipelineState = nullptr );
        void CloseCommandList();
        void ExecuteCommandList();

        ID3D12CommandQueue* GetCommandQueue() const;
        ID3D12CommandAllocator* GetCommandAllocator() const;
        ID3D12GraphicsCommandList* GetCommandList() const;
        ID3D12Fence* GetFence() const;
        UINT64 GetFenceValue() const;
        HANDLE GetFenceEvent() const;
    };
}