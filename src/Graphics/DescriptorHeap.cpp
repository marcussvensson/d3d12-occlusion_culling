#include "DescriptorHeap.h"
#include "GraphicsDefines.h"

namespace Graphics
{
    extern ID3D12Device* gDevice;


    void DescriptorHeap::Create( UINT size, D3D12_DESCRIPTOR_HEAP_TYPE type, D3D12_DESCRIPTOR_HEAP_FLAGS flags )
    {
        D3D12_DESCRIPTOR_HEAP_DESC shaderHeapDesc;
        ZeroMemory( &shaderHeapDesc, sizeof( shaderHeapDesc ) );
        shaderHeapDesc.NumDescriptors = size;
        shaderHeapDesc.Type = type;
        shaderHeapDesc.Flags = flags;
        HR( gDevice->CreateDescriptorHeap( &shaderHeapDesc, IID_PPV_ARGS( &mHeap ) ) );

        mCapacity = size;
        mSize = 0;
        mOffset = gDevice->GetDescriptorHandleIncrementSize( type );
    }

    void DescriptorHeap::Destroy()
    {
        mHeap->Release();
    }

    DescriptorHandle DescriptorHeap::CreateHandle()
    {
        assert( mSize <= mCapacity );

        DescriptorHandle handle;
        handle.cpu = CD3DX12_CPU_DESCRIPTOR_HANDLE( mHeap->GetCPUDescriptorHandleForHeapStart(), mSize, mOffset );
        handle.gpu = CD3DX12_GPU_DESCRIPTOR_HANDLE( mHeap->GetGPUDescriptorHandleForHeapStart(), mSize, mOffset );

        ++mSize;

        return handle;
    }

    ID3D12DescriptorHeap* DescriptorHeap::GetHeap() const
    {
        return mHeap;
    }
}