#pragma once

#include <d3d12.h>
#include <d3d12shader.h>

namespace Graphics
{
    class Shader
    {
    private:
        ID3DBlob* mCode;
        ID3D12ShaderReflection* mReflection;

        UINT mInputElementDescCount;
        D3D12_INPUT_ELEMENT_DESC* mInputElementDescArray;

    public:
        Shader( LPCWSTR filepath, const char* entryPoint, const char* target, const D3D_SHADER_MACRO* defines = nullptr );
        ~Shader();

        D3D12_SHADER_BYTECODE GetShaderBytecode();
        D3D12_INPUT_LAYOUT_DESC GetInputLayout();
    };

}