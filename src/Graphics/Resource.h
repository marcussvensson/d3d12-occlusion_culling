#pragma once

#include "IntermediateResource.h"
#include "DescriptorHeap.h"
#include "GraphicsDefines.h"

#include <d3d12.h>
#include <d3dx12.h>
#include <DirectXMath.h>
#include <SDL_ttf.h>

namespace Graphics
{
    extern ID3D12Device* gDevice;


    class Resource
    {
    protected:
        ID3D12Resource* mDefaultResource;
        D3D12_RESOURCE_STATES mDefaultState;
        UINT mDefaultSize;

        IntermediateResource* mUploadResource;
        IntermediateResource* mReadbackResource;

        DescriptorHandle* mSrvDescriptorHandle;
        DescriptorHandle* mUavDescriptorHandle;

    public:
        Resource();

        virtual void Destroy();

        virtual void Write( ID3D12GraphicsCommandList* commandList ) = 0;
        virtual void Read( ID3D12GraphicsCommandList* commandList ) = 0;

        void Transition( ID3D12GraphicsCommandList* commandList, D3D12_RESOURCE_STATES state );

        ID3D12Resource* GetDefaultResource() const;

        IntermediateResource* GetUpload() const;
        IntermediateResource* GetReadback() const;

        const DescriptorHandle* GetSrvDescriptorHandle() const;
        const DescriptorHandle* GetUavDescriptorHandle() const;
    };

    typedef struct BUFFER_RESOURCE_DESC
    {
        BOOL DefaultEnable;
        BOOL UploadEnable;
        BOOL ReadbackEnable;

        D3D12_RESOURCE_STATES DefaultState;

        DescriptorHandle SrvDescriptorHandle;
        DescriptorHandle UavDescriptorHandle;
        DescriptorHandle CbvDescriptorHandle;

        BOOL UavCounterEnable;
    } BUFFER_RESOURCE_DESC;

    template < class T >
    class BufferResource : public Resource
    {
    protected:
        UINT mElementCount;

        DescriptorHandle* mCbvDescriptorHandle;

    public:
        BufferResource();

        void Create( BUFFER_RESOURCE_DESC* resourceDesc, UINT elementCount = 1 );
        void Destroy();

        void Write( ID3D12GraphicsCommandList* commandList );
        void Read( ID3D12GraphicsCommandList* commandList );

        T* MapUpload();
        T* MapReadback();

        const DescriptorHandle* GetCbvDescriptorHandle() const;
    };

    typedef struct TEXTURE2D_RESOURCE_DESC
    {
        BOOL DefaultEnable;
        BOOL UploadEnable;
        BOOL ReadbackEnable;

        D3D12_RESOURCE_STATES DefaultState;

        DescriptorHandle SrvDescriptorHandle;
        DescriptorHandle UavDescriptorHandle;
        DescriptorHandle DsvDescriptorHandle;
    } TEXTURE2D_RESOURCE_DESC;

    class Texture2DResource : public Resource
    {
    protected:
        DXGI_FORMAT mFormat;
        D3D12_PLACED_SUBRESOURCE_FOOTPRINT mLayout;

        DescriptorHandle* mDsvDescriptorHandle;

    public:
        Texture2DResource();

        void Create( TEXTURE2D_RESOURCE_DESC* resourceDesc, UINT width, UINT height, DXGI_FORMAT format );
        void Create( TEXTURE2D_RESOURCE_DESC* resourceDesc, const char* textureFilepath );
        virtual void Destroy();

        void Write( ID3D12GraphicsCommandList* commandList );
        void Read( ID3D12GraphicsCommandList* commandList );

        const DescriptorHandle* GetDsvDescriptorHandle() const;
    };

    class TextResource : public Texture2DResource
    {
    private:
        TTF_Font* mFont;
        SDL_Color mFontColor;

    public:
        TextResource();

        void Destroy();

        void OpenFont( const char* filepath, int size, SDL_Color color );
        void UpdateText( const char* text );

        const DirectX::XMMATRIX GetScaleMatrix();
    };


    template< class T>
    BufferResource< T >::BufferResource()
        : mElementCount( 0 ),
          mCbvDescriptorHandle( nullptr )
    {
    }

    template< class T>
    void BufferResource< T >::Create( BUFFER_RESOURCE_DESC* resourceDesc, UINT elementCount )
    {
        assert( resourceDesc != nullptr && elementCount > 0 );
        assert( resourceDesc->UavDescriptorHandle.cpu.ptr != 0 || resourceDesc->UavCounterEnable == FALSE );

        UINT elementSize = sizeof( T );
        if ( resourceDesc->CbvDescriptorHandle.cpu.ptr != 0 )
        {
            elementSize = ( elementSize + 255 ) & ~255;
        }

        mElementCount = elementCount;
        mDefaultSize = elementSize * elementCount;
        mDefaultState = resourceDesc->DefaultState;

        UINT bufferSize = mDefaultSize;
        if ( resourceDesc->UavCounterEnable )
        {
            UINT misalignment = bufferSize % D3D12_UAV_COUNTER_PLACEMENT_ALIGNMENT;
            bufferSize += misalignment > 0 ? D3D12_UAV_COUNTER_PLACEMENT_ALIGNMENT - misalignment : 0;
            bufferSize += sizeof( UINT );
        }

        if ( resourceDesc->DefaultEnable )
        {
            D3D12_RESOURCE_FLAGS resourceFlags;
            switch ( mDefaultState )
            {
                case D3D12_RESOURCE_STATE_UNORDERED_ACCESS:
                    resourceFlags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
                    break;
                case D3D12_RESOURCE_STATE_DEPTH_READ:
                case D3D12_RESOURCE_STATE_DEPTH_WRITE:
                    resourceFlags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
                    break;
                default:
                    resourceFlags = D3D12_RESOURCE_FLAG_NONE;
                    break;
            }
            HR( gDevice->CreateCommittedResource(
                &CD3DX12_HEAP_PROPERTIES( D3D12_HEAP_TYPE_DEFAULT ),
                D3D12_HEAP_FLAG_NONE,
                &CD3DX12_RESOURCE_DESC::Buffer( bufferSize, resourceFlags ),
                mDefaultState,
                nullptr,
                IID_PPV_ARGS( &mDefaultResource ) ) );
        }

        if ( resourceDesc->UploadEnable )
        {
            mUploadResource = new IntermediateResource();
            mUploadResource->Create( D3D12_HEAP_TYPE_UPLOAD, bufferSize );
        }

        if ( resourceDesc->ReadbackEnable )
        {
            mReadbackResource = new IntermediateResource();
            mReadbackResource->Create( D3D12_HEAP_TYPE_READBACK, bufferSize );
        }

        if ( resourceDesc->SrvDescriptorHandle.cpu.ptr != 0 )
        {
            assert( mDefaultResource != nullptr );

            mSrvDescriptorHandle = new DescriptorHandle( resourceDesc->SrvDescriptorHandle );

            D3D12_SHADER_RESOURCE_VIEW_DESC viewDesc;
            ZeroMemory( &viewDesc, sizeof( viewDesc ) );
            viewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
            viewDesc.Format = DXGI_FORMAT_UNKNOWN;
            viewDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
            viewDesc.Buffer.FirstElement = 0;
            viewDesc.Buffer.NumElements = mElementCount;
            viewDesc.Buffer.StructureByteStride = sizeof( T );
            viewDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
            gDevice->CreateShaderResourceView( mDefaultResource, &viewDesc, mSrvDescriptorHandle->cpu );
        }

        if ( resourceDesc->UavDescriptorHandle.cpu.ptr != 0 )
        {
            assert( mDefaultResource != nullptr );

            mUavDescriptorHandle = new DescriptorHandle( resourceDesc->UavDescriptorHandle );

            D3D12_UNORDERED_ACCESS_VIEW_DESC viewDesc;
            ZeroMemory( &viewDesc, sizeof( viewDesc ) );
            viewDesc.Format = DXGI_FORMAT_UNKNOWN;
            viewDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
            viewDesc.Buffer.FirstElement = 0;
            viewDesc.Buffer.NumElements = mElementCount;
            viewDesc.Buffer.StructureByteStride = sizeof( T );
            viewDesc.Buffer.CounterOffsetInBytes = resourceDesc->UavCounterEnable ? bufferSize - 4 : 0;
            viewDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;
            gDevice->CreateUnorderedAccessView( mDefaultResource, resourceDesc->UavCounterEnable ? mDefaultResource : nullptr, &viewDesc, mUavDescriptorHandle->cpu );
        }

        if ( resourceDesc->CbvDescriptorHandle.cpu.ptr != 0 )
        {
            assert( mUploadResource != nullptr );

            mCbvDescriptorHandle = new DescriptorHandle( resourceDesc->CbvDescriptorHandle );

            D3D12_CONSTANT_BUFFER_VIEW_DESC viewDesc;
            ZeroMemory( &viewDesc, sizeof( viewDesc ) );
            viewDesc.BufferLocation = mUploadResource->GetResource()->GetGPUVirtualAddress();
            viewDesc.SizeInBytes = mDefaultSize;
            gDevice->CreateConstantBufferView( &viewDesc, mCbvDescriptorHandle->cpu );
        }
    }

    template< class T>
    void BufferResource< T >::Destroy()
    {
        Resource::Destroy();

        if ( mCbvDescriptorHandle != nullptr )
        {
            delete mCbvDescriptorHandle;
            mCbvDescriptorHandle = nullptr;
        }
    }

    template< class T>
    void BufferResource< T >::Write( ID3D12GraphicsCommandList* commandList )
    {
        assert( mDefaultResource != nullptr && mUploadResource != nullptr );
        mUploadResource->CopyBuffer( commandList, mDefaultResource, mDefaultState );
    }
    template< class T>
    void BufferResource< T >::Read( ID3D12GraphicsCommandList* commandList )
    {
        assert( mDefaultResource != nullptr && mReadbackResource != nullptr );
        mReadbackResource->CopyBuffer( commandList, mDefaultResource, mDefaultState );
    }

    template< class T>
    T* BufferResource< T >::MapUpload()
    {
        return reinterpret_cast< T* >( mUploadResource->Map() );
    }

    template< class T>
    T* BufferResource< T >::MapReadback()
    {
        return reinterpret_cast< T* >( mReadbackResource->Map() );
    }

    template< class T>
    const DescriptorHandle* BufferResource< T >::GetCbvDescriptorHandle() const
    {
        return mCbvDescriptorHandle;
    }
}