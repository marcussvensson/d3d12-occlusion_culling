#include "IntermediateResource.h"
#include "GraphicsDefines.h"

#include <stdio.h>
#include <assert.h>

namespace Graphics
{
    extern ID3D12Device* gDevice;


    IntermediateResource::IntermediateResource()
        : mResource( nullptr ), mMappedData( nullptr )
    {
    }

    void IntermediateResource::Create( D3D12_HEAP_TYPE heapType, UINT size )
    {
        assert( mResource == nullptr );
        assert( heapType == D3D12_HEAP_TYPE_UPLOAD || heapType == D3D12_HEAP_TYPE_READBACK );

        mHeapType = heapType;
        mSize = size;

        D3D12_RESOURCE_STATES state;
        switch ( mHeapType )
        {
            case D3D12_HEAP_TYPE_UPLOAD:
                state = D3D12_RESOURCE_STATE_GENERIC_READ;
                break;
            case D3D12_HEAP_TYPE_READBACK:
                state = D3D12_RESOURCE_STATE_COPY_DEST;
                break;
        }

        HR( gDevice->CreateCommittedResource(
            &CD3DX12_HEAP_PROPERTIES( mHeapType ),
            D3D12_HEAP_FLAG_NONE,
            &CD3DX12_RESOURCE_DESC::Buffer( mSize, D3D12_RESOURCE_FLAG_NONE ),
            state,
            nullptr,
            IID_PPV_ARGS( &mResource ) ) );
    }

    void IntermediateResource::Destroy()
    {
        Unmap();
        
        if ( mResource != nullptr )
        {
            mResource->Release();
            mResource = nullptr;
        }
    }

    void IntermediateResource::CopyBuffer( ID3D12GraphicsCommandList* commandList,
                                           ID3D12Resource* defaultResource, D3D12_RESOURCE_STATES defaultState )
    {
        switch ( mHeapType )
        {
            case D3D12_HEAP_TYPE_UPLOAD:
                CopyBuffer( commandList, defaultResource, mResource, defaultState, D3D12_RESOURCE_STATE_COPY_DEST );
                break;
            case D3D12_HEAP_TYPE_READBACK:
                CopyBuffer( commandList, mResource, defaultResource, defaultState, D3D12_RESOURCE_STATE_COPY_SOURCE );
                break;
        }
    }

    void IntermediateResource::CopyTexture2D( ID3D12GraphicsCommandList* commandList,
                                              ID3D12Resource* defaultResource, D3D12_RESOURCE_STATES defaultState,
                                              D3D12_PLACED_SUBRESOURCE_FOOTPRINT& layout )
    {
        switch ( mHeapType )
        {
            case D3D12_HEAP_TYPE_UPLOAD:
                CopyTexture2D( commandList, defaultResource, mResource, defaultState, D3D12_RESOURCE_STATE_COPY_DEST, layout );
                break;
            case D3D12_HEAP_TYPE_READBACK:
                CopyTexture2D( commandList, mResource, defaultResource, defaultState, D3D12_RESOURCE_STATE_COPY_SOURCE, layout );
                break;
        }
    }

    BYTE* IntermediateResource::Map()
    {
        if ( mMappedData == nullptr )
            HR( mResource->Map( 0, &CD3DX12_RANGE( 0, 0 ), reinterpret_cast< void** >( &mMappedData ) ) );
        return mMappedData;
    }

    void IntermediateResource::Unmap()
    {
        if ( mMappedData != nullptr )
        {
            mResource->Unmap( 0, &CD3DX12_RANGE( 0, 0 ) );
            mMappedData = nullptr;
        }
    }

    ID3D12Resource* IntermediateResource::GetResource() const
    {
        return mResource;
    }

    const UINT IntermediateResource::GetSize() const
    {
        return mSize;
    }

    void IntermediateResource::CopyBuffer( ID3D12GraphicsCommandList* commandList,
                                           ID3D12Resource* destination, ID3D12Resource* source,
                                           D3D12_RESOURCE_STATES defaultState, D3D12_RESOURCE_STATES copyState )
    {
        assert( copyState == D3D12_RESOURCE_STATE_COPY_DEST || copyState == D3D12_RESOURCE_STATE_COPY_SOURCE );

        ID3D12Resource* defaultResource = copyState == D3D12_RESOURCE_STATE_COPY_DEST ? destination : source;

        commandList->ResourceBarrier( 1, &CD3DX12_RESOURCE_BARRIER::Transition( defaultResource, defaultState, copyState ) );
        commandList->CopyBufferRegion( destination, 0, source, 0, mSize );
        commandList->ResourceBarrier( 1, &CD3DX12_RESOURCE_BARRIER::Transition( defaultResource, copyState, defaultState ) );
    }

    void IntermediateResource::CopyTexture2D( ID3D12GraphicsCommandList* commandList,
                                              ID3D12Resource* destination, ID3D12Resource* source,
                                              D3D12_RESOURCE_STATES defaultState, D3D12_RESOURCE_STATES copyState,
                                              D3D12_PLACED_SUBRESOURCE_FOOTPRINT& layout )
    {
        assert( copyState == D3D12_RESOURCE_STATE_COPY_DEST || copyState == D3D12_RESOURCE_STATE_COPY_SOURCE );

        ID3D12Resource* defaultResource = copyState == D3D12_RESOURCE_STATE_COPY_DEST ? destination : source;
        CD3DX12_TEXTURE_COPY_LOCATION destinationLocation( destination, 0 );
        CD3DX12_TEXTURE_COPY_LOCATION sourceLocation( source, layout );

        commandList->ResourceBarrier( 1, &CD3DX12_RESOURCE_BARRIER::Transition( defaultResource, defaultState, copyState ) );
        commandList->CopyTextureRegion( &destinationLocation, 0, 0, 0, &sourceLocation, nullptr );
        commandList->ResourceBarrier( 1, &CD3DX12_RESOURCE_BARRIER::Transition( defaultResource, copyState, defaultState ) );
    }
}