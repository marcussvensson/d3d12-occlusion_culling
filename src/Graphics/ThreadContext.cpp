#include "ThreadContext.h"
#include "GraphicsDefines.h"

namespace Graphics
{
    extern ID3D12Device* gDevice;


    void ThreadContext::Create( LPTHREAD_START_ROUTINE function, LPVOID parameter )
    {
        HR( gDevice->CreateFence( 0, D3D12_FENCE_FLAG_SHARED, IID_PPV_ARGS( &mFence ) ) );
        mFenceValue = 1;
        mFenceEvent = CreateEventEx( nullptr, FALSE, FALSE, EVENT_ALL_ACCESS );

        mThread = CreateThread( nullptr, 0, function, parameter, CREATE_SUSPENDED, nullptr );
        ResumeThread( mThread );
    }

    void ThreadContext::Destroy()
    {
        WaitForSingleObjectEx( mThread, INFINITE, FALSE );
        CloseHandle( mThread );

        CloseHandle( mFenceEvent );

        mFence->Release();
    }

    void ThreadContext::Signal()
    {
        mFence->Signal( mFenceValue++ );
    }

    void ThreadContext::Wait()
    {
        if ( mFence->GetCompletedValue() < mFenceValue )
        {
            mFence->SetEventOnCompletion( mFenceValue, mFenceEvent );
            WaitForSingleObject( mFenceEvent, INFINITE );
        }
    }
}