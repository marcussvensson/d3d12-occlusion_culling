#include "CommandContext.h"
#include "GraphicsDefines.h"

namespace Graphics
{
    extern ID3D12Device* gDevice;


    void CommandContext::Create( D3D12_COMMAND_LIST_TYPE type, D3D12_FENCE_FLAGS fenceFlags )
    {
        D3D12_COMMAND_QUEUE_DESC commandQueueDesc;
        ZeroMemory( &commandQueueDesc, sizeof( commandQueueDesc ) );
        commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
        commandQueueDesc.Type = type;
        HR( gDevice->CreateCommandQueue( &commandQueueDesc, IID_PPV_ARGS( &mCommandQueue ) ) );

        HR( gDevice->CreateCommandAllocator( type, IID_PPV_ARGS( &mCommandAllocator ) ) );

        HR( gDevice->CreateCommandList( 0, type, mCommandAllocator, nullptr, IID_PPV_ARGS( &mCommandList ) ) );

        HR( gDevice->CreateFence( 0, fenceFlags, IID_PPV_ARGS( &mFence ) ) );
        mFenceValue = 1;
        mFenceEvent = CreateEventEx( nullptr, FALSE, FALSE, EVENT_ALL_ACCESS );
    }

    void CommandContext::Destroy()
    {
        CloseHandle( mFenceEvent );

        mFence->Release();
        mCommandList->Release();
        mCommandAllocator->Release();
        mCommandQueue->Release();
    }

    void CommandContext::Wait( ID3D12Fence* fence, UINT64 fenceValue )
    {
        if ( fence->GetCompletedValue() < fenceValue )
        {
            HR( mCommandQueue->Wait( fence, fenceValue ) );
        }
    }

    void CommandContext::Wait( CommandContext& commandContext )
    {
        commandContext.mCommandQueue->Signal( commandContext.mFence, commandContext.mFenceValue );
        Wait( commandContext.mFence, commandContext.mFenceValue );
        ++commandContext.mFenceValue;
    }

    void CommandContext::Wait( ThreadContext& threadContext )
    {
        Wait( threadContext.mFence, threadContext.mFenceValue );
    }

    void CommandContext::WaitForGpu()
    {
        mCommandQueue->Signal( mFence, mFenceValue );

        if ( mFence->GetCompletedValue() < mFenceValue )
        {
            mFence->SetEventOnCompletion( mFenceValue, mFenceEvent );
            WaitForSingleObject( mFenceEvent, INFINITE );
        }

        ++mFenceValue;
    }

    void CommandContext::ResetCommandList( ID3D12PipelineState* initialPipelineState )
    {
        HR( mCommandAllocator->Reset() );
        HR( mCommandList->Reset( mCommandAllocator, initialPipelineState ) );
    }

    void CommandContext::CloseCommandList()
    {
        HR( mCommandList->Close() );
    }

    void CommandContext::ExecuteCommandList()
    {
        ID3D12CommandList* commandLists[] = { mCommandList };
        mCommandQueue->ExecuteCommandLists( _countof( commandLists ), commandLists );
    }

    ID3D12CommandQueue* CommandContext::GetCommandQueue() const
    {
        return mCommandQueue;
    }

    ID3D12CommandAllocator* CommandContext::GetCommandAllocator() const
    {
        return mCommandAllocator;
    }

    ID3D12GraphicsCommandList* CommandContext::GetCommandList() const
    {
        return mCommandList;
    }

    ID3D12Fence* CommandContext::GetFence() const
    {
        return mFence;
    }

    UINT64 CommandContext::GetFenceValue() const
    {
        return mFenceValue;
    }

    HANDLE CommandContext::GetFenceEvent() const
    {
        return mFenceEvent;
    }
}