#include "Shader.h"

#include <d3dcompiler.h>
#include <stdio.h>
#include <assert.h>

namespace Graphics
{
    Shader::Shader( LPCWSTR filepath, const char* entryPoint, const char* target, const D3D_SHADER_MACRO* defines )
        : mCode( nullptr ), mReflection( nullptr ), mInputElementDescCount( 0 ), mInputElementDescArray( nullptr )
    {
        UINT compileFlags = D3DCOMPILE_OPTIMIZATION_LEVEL3;
    #ifdef _DEBUG
        compileFlags = D3DCOMPILE_DEBUG;
    #endif

        ID3DBlob* error = nullptr;
        HRESULT hr = D3DCompileFromFile( filepath, defines, D3D_COMPILE_STANDARD_FILE_INCLUDE, entryPoint, target, compileFlags, 0, &mCode, &error );
        if ( FAILED( hr ) )
        {
            if ( hr == HRESULT_FROM_WIN32( ERROR_FILE_NOT_FOUND ) )
                printf( "File %ws not found.\n", filepath );
            else
                printf( "Error compiling file %ws: %s\n", filepath, ( char* )error->GetBufferPointer() );
            throw;
        }
        if ( error != nullptr )
        {
            error->Release();
        }

        D3DReflect( mCode->GetBufferPointer(), mCode->GetBufferSize(), IID_PPV_ARGS( &mReflection ) );

        D3D12_SHADER_DESC shaderDesc;
        mReflection->GetDesc( &shaderDesc );

        mInputElementDescCount = shaderDesc.InputParameters;
        mInputElementDescArray = new D3D12_INPUT_ELEMENT_DESC[ mInputElementDescCount ];
        for ( UINT i = 0; i < shaderDesc.InputParameters; ++i )
        {
            D3D12_SIGNATURE_PARAMETER_DESC parameterDesc;
            mReflection->GetInputParameterDesc( i, &parameterDesc );

            ZeroMemory( &mInputElementDescArray[ i ], sizeof( mInputElementDescArray[ i ] ) );
            mInputElementDescArray[ i ].SemanticName = parameterDesc.SemanticName;
            mInputElementDescArray[ i ].SemanticIndex = parameterDesc.SemanticIndex;
            mInputElementDescArray[ i ].AlignedByteOffset = D3D12_APPEND_ALIGNED_ELEMENT;

            if ( parameterDesc.Mask == 1 )
            {
                if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32_UINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32_SINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32_FLOAT;
            }
            else if ( parameterDesc.Mask <= 3 )
            {
                if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32_UINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32_SINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32_FLOAT;
            }
            else if ( parameterDesc.Mask <= 7 )
            {
                if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32B32_UINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32B32_SINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32B32_FLOAT;
            }
            else if ( parameterDesc.Mask <= 15 )
            {
                if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_UINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32B32A32_UINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_SINT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32B32A32_SINT;
                else if ( parameterDesc.ComponentType == D3D_REGISTER_COMPONENT_FLOAT32 )
                    mInputElementDescArray[ i ].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
            }
        }
    }

    Shader::~Shader()
    {
        mInputElementDescCount = 0;

        if ( mInputElementDescArray != nullptr )
        {
            delete[] mInputElementDescArray;
            mInputElementDescArray = nullptr;
        }
        if ( mReflection != nullptr )
        {
            mReflection->Release();
            mReflection = nullptr;
        }
        if ( mCode != nullptr )
        {
            mCode->Release();
            mCode = nullptr;
        }
    }

    D3D12_SHADER_BYTECODE Shader::GetShaderBytecode()
    {
        return { mCode->GetBufferPointer(), static_cast< UINT >( mCode->GetBufferSize() ) };
    }

    D3D12_INPUT_LAYOUT_DESC Shader::GetInputLayout()
    {
        return { mInputElementDescArray, mInputElementDescCount };
    }
}