#pragma once

#include <d3d12.h>
#include <d3dx12.h>

namespace Graphics
{
    struct DescriptorHandle
    {
        CD3DX12_CPU_DESCRIPTOR_HANDLE cpu;
        CD3DX12_GPU_DESCRIPTOR_HANDLE gpu;
    };

    class DescriptorHeap
    {
    private:
        ID3D12DescriptorHeap* mHeap;
        UINT mCapacity;
        UINT mSize;
        UINT mOffset;

    public:
        void Create( UINT size, D3D12_DESCRIPTOR_HEAP_TYPE type, D3D12_DESCRIPTOR_HEAP_FLAGS flags );
        void Destroy();

        DescriptorHandle CreateHandle();

        ID3D12DescriptorHeap* GetHeap() const;
    };
}