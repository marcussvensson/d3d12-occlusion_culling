#pragma once

#include <SDL.h>

namespace Graphics
{
    class Timer
    {
    private:
        Uint64 mPreviousTimestamp;
        float mCountsPerSecond;

        float mDeltaTime;
        float mFramesPerSecond;

    public:
        Timer();

        void Tick();

        const float GetDeltaTime() const;
        const float GetFramesPerSecond() const;
    };
}