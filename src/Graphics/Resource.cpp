#include "Resource.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include <stdio.h>
#include <assert.h>

namespace Graphics
{
    extern ID3D12Device* gDevice;
    extern D3D12_VIEWPORT gViewport;


    Resource::Resource()
        : mDefaultResource( nullptr ), mDefaultSize( 0 ),
          mUploadResource( nullptr ), mReadbackResource( nullptr ),
          mSrvDescriptorHandle( nullptr ), mUavDescriptorHandle( nullptr )
    {
    }

    void Resource::Destroy()
    {
        if ( mDefaultResource != nullptr )
        {
            mDefaultResource->Release();
            mDefaultResource = nullptr;
        }

        if ( mUploadResource != nullptr )
        {
            mUploadResource->Destroy();
            delete mUploadResource;
            mUploadResource = nullptr;
        }
        if ( mReadbackResource != nullptr )
        {
            mReadbackResource->Destroy();
            delete mReadbackResource;
            mReadbackResource = nullptr;
        }

        if ( mSrvDescriptorHandle != nullptr )
        {
            delete mSrvDescriptorHandle;
            mSrvDescriptorHandle = nullptr;
        }
        if ( mUavDescriptorHandle != nullptr )
        {
            delete mUavDescriptorHandle;
            mUavDescriptorHandle = nullptr;
        }
    }

    ID3D12Resource* Resource::GetDefaultResource() const
    {
        return mDefaultResource;
    }

    IntermediateResource* Resource::GetUpload() const
    {
        return mUploadResource;
    }
    IntermediateResource* Resource::GetReadback() const
    {
        return mReadbackResource;
    }

    void Resource::Transition( ID3D12GraphicsCommandList* commandList, D3D12_RESOURCE_STATES state )
    {
        commandList->ResourceBarrier( 1, &CD3DX12_RESOURCE_BARRIER::Transition( mDefaultResource, mDefaultState, state ) );
        mDefaultState = state;
    }

    const DescriptorHandle* Resource::GetSrvDescriptorHandle() const
    {
        return mSrvDescriptorHandle;
    }
    const DescriptorHandle* Resource::GetUavDescriptorHandle() const
    {
        return mUavDescriptorHandle;
    }

    Texture2DResource::Texture2DResource()
        : mDsvDescriptorHandle( nullptr )
    {
    }

    void Texture2DResource::Create( TEXTURE2D_RESOURCE_DESC* resourceDesc, UINT width, UINT height, DXGI_FORMAT format )
    {
        assert( resourceDesc != nullptr && width > 0 && height > 0 );

        mFormat = format;
        mDefaultState = resourceDesc->DefaultState;

        if ( resourceDesc->DefaultEnable )
        {
            UINT elementSize;
            switch ( mFormat )
            {
                case DXGI_FORMAT_R32G32B32A32_TYPELESS:
                case DXGI_FORMAT_R32G32B32A32_FLOAT:
                case DXGI_FORMAT_R32G32B32A32_UINT:
                case DXGI_FORMAT_R32G32B32A32_SINT:
                    elementSize = 16;
                    break;
                case DXGI_FORMAT_R32G32B32_TYPELESS:
                case DXGI_FORMAT_R32G32B32_FLOAT:
                case DXGI_FORMAT_R32G32B32_UINT:
                case DXGI_FORMAT_R32G32B32_SINT:
                    elementSize = 12;
                    break;
                case DXGI_FORMAT_R16G16B16A16_TYPELESS:
                case DXGI_FORMAT_R16G16B16A16_FLOAT:
                case DXGI_FORMAT_R16G16B16A16_UNORM:
                case DXGI_FORMAT_R16G16B16A16_UINT:
                case DXGI_FORMAT_R16G16B16A16_SNORM:
                case DXGI_FORMAT_R16G16B16A16_SINT:
                case DXGI_FORMAT_R32G32_TYPELESS:
                case DXGI_FORMAT_R32G32_FLOAT:
                case DXGI_FORMAT_R32G32_UINT:
                case DXGI_FORMAT_R32G32_SINT:
                case DXGI_FORMAT_R32G8X24_TYPELESS:
                case DXGI_FORMAT_D32_FLOAT_S8X24_UINT:
                case DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS:
                case DXGI_FORMAT_X32_TYPELESS_G8X24_UINT:
                    elementSize = 8;
                    break;
                case DXGI_FORMAT_R10G10B10A2_TYPELESS:
                case DXGI_FORMAT_R10G10B10A2_UNORM:
                case DXGI_FORMAT_R10G10B10A2_UINT:
                case DXGI_FORMAT_R11G11B10_FLOAT:
                case DXGI_FORMAT_R8G8B8A8_TYPELESS:
                case DXGI_FORMAT_R8G8B8A8_UNORM:
                case DXGI_FORMAT_R8G8B8A8_UNORM_SRGB:
                case DXGI_FORMAT_R8G8B8A8_UINT:
                case DXGI_FORMAT_R8G8B8A8_SNORM:
                case DXGI_FORMAT_R8G8B8A8_SINT:
                case DXGI_FORMAT_R16G16_TYPELESS:
                case DXGI_FORMAT_R16G16_FLOAT:
                case DXGI_FORMAT_R16G16_UNORM:
                case DXGI_FORMAT_R16G16_UINT:
                case DXGI_FORMAT_R16G16_SNORM:
                case DXGI_FORMAT_R16G16_SINT:
                case DXGI_FORMAT_R32_TYPELESS:
                case DXGI_FORMAT_D32_FLOAT:
                case DXGI_FORMAT_R32_FLOAT:
                case DXGI_FORMAT_R32_UINT:
                case DXGI_FORMAT_R32_SINT:
                case DXGI_FORMAT_R24G8_TYPELESS:
                case DXGI_FORMAT_D24_UNORM_S8_UINT:
                case DXGI_FORMAT_R24_UNORM_X8_TYPELESS:
                case DXGI_FORMAT_X24_TYPELESS_G8_UINT:
                case DXGI_FORMAT_R9G9B9E5_SHAREDEXP:
                case DXGI_FORMAT_R8G8_B8G8_UNORM:
                case DXGI_FORMAT_G8R8_G8B8_UNORM:
                case DXGI_FORMAT_B8G8R8A8_UNORM:
                case DXGI_FORMAT_B8G8R8X8_UNORM:
                case DXGI_FORMAT_R10G10B10_XR_BIAS_A2_UNORM:
                case DXGI_FORMAT_B8G8R8A8_TYPELESS:
                case DXGI_FORMAT_B8G8R8A8_UNORM_SRGB:
                case DXGI_FORMAT_B8G8R8X8_TYPELESS:
                case DXGI_FORMAT_B8G8R8X8_UNORM_SRGB:
                    elementSize = 4;
                    break;
                case DXGI_FORMAT_R8G8_TYPELESS:
                case DXGI_FORMAT_R8G8_UNORM:
                case DXGI_FORMAT_R8G8_UINT:
                case DXGI_FORMAT_R8G8_SNORM:
                case DXGI_FORMAT_R8G8_SINT:
                case DXGI_FORMAT_R16_TYPELESS:
                case DXGI_FORMAT_R16_FLOAT:
                case DXGI_FORMAT_D16_UNORM:
                case DXGI_FORMAT_R16_UNORM:
                case DXGI_FORMAT_R16_UINT:
                case DXGI_FORMAT_R16_SNORM:
                case DXGI_FORMAT_R16_SINT:
                case DXGI_FORMAT_B5G6R5_UNORM:
                case DXGI_FORMAT_B5G5R5A1_UNORM:
                case DXGI_FORMAT_B4G4R4A4_UNORM:
                    elementSize = 2;
                    break;
                case DXGI_FORMAT_R8_TYPELESS:
                case DXGI_FORMAT_R8_UNORM:
                case DXGI_FORMAT_R8_UINT:
                case DXGI_FORMAT_R8_SNORM:
                case DXGI_FORMAT_R8_SINT:
                case DXGI_FORMAT_A8_UNORM:
                case DXGI_FORMAT_R1_UNORM:
                case DXGI_FORMAT_BC1_TYPELESS:
                case DXGI_FORMAT_BC1_UNORM:
                case DXGI_FORMAT_BC1_UNORM_SRGB:
                case DXGI_FORMAT_BC4_TYPELESS:
                case DXGI_FORMAT_BC4_UNORM:
                case DXGI_FORMAT_BC4_SNORM:
                case DXGI_FORMAT_BC2_TYPELESS:
                case DXGI_FORMAT_BC2_UNORM:
                case DXGI_FORMAT_BC2_UNORM_SRGB:
                case DXGI_FORMAT_BC3_TYPELESS:
                case DXGI_FORMAT_BC3_UNORM:
                case DXGI_FORMAT_BC3_UNORM_SRGB:
                case DXGI_FORMAT_BC5_TYPELESS:
                case DXGI_FORMAT_BC5_UNORM:
                case DXGI_FORMAT_BC5_SNORM:
                case DXGI_FORMAT_BC6H_TYPELESS:
                case DXGI_FORMAT_BC6H_UF16:
                case DXGI_FORMAT_BC6H_SF16:
                case DXGI_FORMAT_BC7_TYPELESS:
                case DXGI_FORMAT_BC7_UNORM:
                case DXGI_FORMAT_BC7_UNORM_SRGB:
                    elementSize = 1;
                    break;
                default:
                    elementSize = 0;
                    break;
            }

            mLayout.Offset = 0;
            mLayout.Footprint.Format = mFormat;
            mLayout.Footprint.RowPitch = width * elementSize;
            mLayout.Footprint.Width = width;
            mLayout.Footprint.Height = height;
            mLayout.Footprint.Depth = 1;

            mDefaultSize = mLayout.Footprint.RowPitch * mLayout.Footprint.Height;

            D3D12_RESOURCE_FLAGS resourceFlags;
            switch ( mDefaultState )
            {
                case D3D12_RESOURCE_STATE_UNORDERED_ACCESS:
                    resourceFlags = D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS;
                    break;
                case D3D12_RESOURCE_STATE_DEPTH_READ:
                case D3D12_RESOURCE_STATE_DEPTH_WRITE:
                    resourceFlags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
                    break;
                default:
                    resourceFlags = D3D12_RESOURCE_FLAG_NONE;
                    break;
            }

            D3D12_CLEAR_VALUE* clearValue = nullptr;
            if ( resourceDesc->DsvDescriptorHandle.cpu.ptr != 0 )
            {
                clearValue = new D3D12_CLEAR_VALUE();
                clearValue->Format = mFormat;
                clearValue->DepthStencil.Depth = 1.0f;
                clearValue->DepthStencil.Stencil = 0;
            }

            HR( gDevice->CreateCommittedResource(
                &CD3DX12_HEAP_PROPERTIES( D3D12_HEAP_TYPE_DEFAULT ),
                D3D12_HEAP_FLAG_NONE,
                &CD3DX12_RESOURCE_DESC(
                    D3D12_RESOURCE_DIMENSION_TEXTURE2D, 0, width, height, 1, 1,
                    mFormat, 1, 0, D3D12_TEXTURE_LAYOUT_UNKNOWN, resourceFlags ),
                mDefaultState,
                clearValue,
                IID_PPV_ARGS( &mDefaultResource ) ) );

            if ( clearValue != nullptr )
            {
                delete clearValue;
            }
        }

        if ( resourceDesc->UploadEnable )
        {
            mUploadResource = new IntermediateResource();
            mUploadResource->Create( D3D12_HEAP_TYPE_UPLOAD, mDefaultSize * 2 );
        }

        if ( resourceDesc->ReadbackEnable )
        {
            mReadbackResource = new IntermediateResource();
            mReadbackResource->Create( D3D12_HEAP_TYPE_READBACK, mDefaultSize * 2 );
        }

        if ( resourceDesc->SrvDescriptorHandle.cpu.ptr != 0 )
        {
            assert( mDefaultResource != nullptr );

            mSrvDescriptorHandle = new DescriptorHandle( resourceDesc->SrvDescriptorHandle );

            D3D12_SHADER_RESOURCE_VIEW_DESC viewDesc;
            ZeroMemory( &viewDesc, sizeof( viewDesc ) );
            viewDesc.Format = mFormat;
            viewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
            viewDesc.Texture2D.MipLevels = 1;
            viewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
            gDevice->CreateShaderResourceView( mDefaultResource, &viewDesc, mSrvDescriptorHandle->cpu );
        }

        if ( resourceDesc->UavDescriptorHandle.cpu.ptr != 0 )
        {
            assert( mDefaultResource != nullptr );

            mUavDescriptorHandle = new DescriptorHandle( resourceDesc->UavDescriptorHandle );

            D3D12_UNORDERED_ACCESS_VIEW_DESC viewDesc;
            ZeroMemory( &viewDesc, sizeof( viewDesc ) );
            viewDesc.Format = mFormat;
            viewDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
            gDevice->CreateUnorderedAccessView( mDefaultResource, nullptr, &viewDesc, mUavDescriptorHandle->cpu );
        }

        if ( resourceDesc->DsvDescriptorHandle.cpu.ptr != 0 )
        {
            assert( mDefaultResource != nullptr );

            mDsvDescriptorHandle = new DescriptorHandle( resourceDesc->DsvDescriptorHandle );

            D3D12_DEPTH_STENCIL_VIEW_DESC viewDesc;
            ZeroMemory( &viewDesc, sizeof( viewDesc ) );
            viewDesc.Format = mFormat;
            viewDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
            viewDesc.Texture2D.MipSlice = 0;
            gDevice->CreateDepthStencilView( mDefaultResource, &viewDesc, mDsvDescriptorHandle->cpu );
        }
    }

    void Texture2DResource::Create( TEXTURE2D_RESOURCE_DESC* resourceDesc, const char* textureFilepath )
    {
        assert( resourceDesc != nullptr && textureFilepath != nullptr );
        assert( resourceDesc->UploadEnable == TRUE );

        FILE* file;
        if ( fopen_s( &file, textureFilepath, "rb" ) )
            return;

        int width, height, componentCount;
        unsigned char* imageData = stbi_load_from_file( file, &width, &height, &componentCount, 0 );
        assert( componentCount == 4 );

        fclose( file );

        Create( resourceDesc, width, height, DXGI_FORMAT_B8G8R8A8_UNORM );

        BYTE* mappedUploadData = mUploadResource->Map();
        memcpy( mappedUploadData, imageData, mDefaultSize );
        mUploadResource->Unmap();

        stbi_image_free( imageData );
    }

    void Texture2DResource::Destroy()
    {
        Resource::Destroy();

        if ( mDsvDescriptorHandle != nullptr )
        {
            delete mDsvDescriptorHandle;
            mDsvDescriptorHandle = nullptr;
        }
    }

    void Texture2DResource::Write( ID3D12GraphicsCommandList* commandList )
    {
        assert( mDefaultResource != nullptr && mUploadResource != nullptr );
        mUploadResource->CopyTexture2D( commandList, mDefaultResource, mDefaultState, mLayout );
    }
    void Texture2DResource::Read( ID3D12GraphicsCommandList* commandList )
    {
        assert( mDefaultResource != nullptr && mReadbackResource != nullptr );
        mReadbackResource->CopyTexture2D( commandList, mDefaultResource, mDefaultState, mLayout );
    }

    const DescriptorHandle* Texture2DResource::GetDsvDescriptorHandle() const
    {
        return mDsvDescriptorHandle;
    }

    TextResource::TextResource()
        : mFont( nullptr )
    {
    }

    void TextResource::Destroy()
    {
        Texture2DResource::Destroy();

        if ( mFont != nullptr )
        {
            TTF_CloseFont( mFont );
            mFont = nullptr;
        }
    }

    void TextResource::OpenFont( const char* filepath, int size, SDL_Color color )
    {
        assert( mUploadResource != nullptr );

        mFont = TTF_OpenFont( filepath, size );
        mFontColor = color;
    }
    void TextResource::UpdateText( const char* text )
    {
        assert( mDefaultResource != nullptr && mUploadResource != nullptr );

        SDL_Surface* textSurface = TTF_RenderText_Blended_Wrapped( mFont, text, mFontColor, mLayout.Footprint.Width );

        BYTE* mappedUploadData = mUploadResource->Map();
        ZeroMemory( mappedUploadData, mDefaultSize );

        UINT destinationRowPitch = mLayout.Footprint.RowPitch;
        UINT sourceRowPitch = static_cast< UINT >( textSurface->w ) > destinationRowPitch ? destinationRowPitch : textSurface->w * 4;
        UINT rowCount = static_cast< UINT >( textSurface->h ) > mLayout.Footprint.Height ? mLayout.Footprint.Height : textSurface->h;
        for ( UINT i = 0; i < rowCount; ++i )
        {
            BYTE* destinationRow = mappedUploadData + destinationRowPitch * i;
            const BYTE* sourceRow = reinterpret_cast< const BYTE* >( textSurface->pixels ) + sourceRowPitch * i;
            memcpy( destinationRow, sourceRow, sourceRowPitch );
        }

        SDL_FreeSurface( textSurface );
    }

    const DirectX::XMMATRIX TextResource::GetScaleMatrix()
    {
        return DirectX::XMMatrixScaling( mLayout.Footprint.Width / gViewport.Width,
                                         mLayout.Footprint.Height / gViewport.Height,
                                         1.0f );
    }
}