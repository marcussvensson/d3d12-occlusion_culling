#pragma once

#include <SDL.h>
#include <bitset>

namespace Input
{
    void Reset();
    void ProcessEvent( SDL_Event& event );

    const bool IsKeyDown( SDL_Scancode key );
    const bool IsKeyUp( SDL_Scancode key );
    const bool IsKeyPressed( SDL_Scancode key );
    const bool IsKeyReleased( SDL_Scancode key );

    const bool IsMouseButtonDown( Uint8 button );
    const bool IsMouseButtonUp( Uint8 button );
    const bool IsMouseButtonPressed( Uint8 button );
    const bool IsMouseButtonReleased( Uint8 button );

    const Sint32* GetMousePosition();
    const Sint32* GetDeltaMousePosition();
}