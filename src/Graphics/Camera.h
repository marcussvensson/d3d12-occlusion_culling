#pragma once

#include <DirectXMath.h>

namespace Graphics
{
    class Camera
    {
    private:
        DirectX::XMVECTOR mPosition;
        DirectX::XMVECTOR mRight;
        DirectX::XMVECTOR mUp;
        DirectX::XMVECTOR mLook;

        DirectX::XMMATRIX mView;
        DirectX::XMMATRIX mProjection;

    public:
        Camera();

        void Strafe( float d );
        void Walk( float d );
        void Pitch( float angle );
        void Yaw( float angle );

        void UpdateViewMatrix();

        void ExtractFrustumPlanes( DirectX::XMFLOAT4 planes[ 6 ] );

        void SetPosition( DirectX::XMFLOAT3 position );
        void SetPerspective( float fovY, float aspectRatio, float nearZ, float farZ );

        const DirectX::XMMATRIX GetView();
        const DirectX::XMMATRIX GetProjection();
        const DirectX::XMMATRIX GetViewProjection();

        const DirectX::XMFLOAT3 GetPosition();
        const DirectX::XMFLOAT3 GetRight();
        const DirectX::XMFLOAT3 GetUp();
        const DirectX::XMFLOAT3 GetLook();
    };
}