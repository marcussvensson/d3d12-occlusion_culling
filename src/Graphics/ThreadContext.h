#pragma once

#include <d3d12.h>

namespace Graphics
{
    class CommandContext;

    class ThreadContext
    {
        friend class CommandContext;

    private:
        HANDLE mThread;

        ID3D12Fence* mFence;
        UINT64 mFenceValue;
        HANDLE mFenceEvent;

    public:
        void Create( LPTHREAD_START_ROUTINE function, LPVOID parameter );
        void Destroy();

        void Signal();
        void Wait();
    };
}