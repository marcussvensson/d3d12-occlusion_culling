#include "TimestampQueryHeap.h"
#include "CommandContext.h"
#include "GraphicsDefines.h"

#include <assert.h>

namespace Graphics
{
    extern ID3D12Device* gDevice;
    extern CommandContext gGraphicsContext;
    extern UINT8 gSwapIndex;


    TimestampQueryHeap::TimestampQueryHeap()
        : mQueryHeap( nullptr ), mTimestampCount( 0 )
    {
    }

    void TimestampQueryHeap::Create( UINT timestampCount )
    {
        mTimestampCount = timestampCount;

        D3D12_QUERY_HEAP_DESC heapDesc;
        ZeroMemory( &heapDesc, sizeof( heapDesc ) );
        heapDesc.Type = D3D12_QUERY_HEAP_TYPE_TIMESTAMP;
        heapDesc.Count = mTimestampCount * 2;
        HR( gDevice->CreateQueryHeap( &heapDesc, IID_PPV_ARGS( &mQueryHeap ) ) );

        mReadbackResource.Create( D3D12_HEAP_TYPE_READBACK, heapDesc.Count * static_cast< UINT >( sizeof( UINT64 ) ) );

        gGraphicsContext.GetCommandQueue()->GetTimestampFrequency( &mTimestampFrequency );
    }

    void TimestampQueryHeap::Destroy()
    {
        mReadbackResource.Destroy();
        mQueryHeap->Release();
    }

    void TimestampQueryHeap::SetTimestampQuery( ID3D12GraphicsCommandList* commandList, UINT index )
    {
        assert( index < mTimestampCount );
        UINT queryIndex = index + gSwapIndex * mTimestampCount;
        commandList->EndQuery( mQueryHeap, D3D12_QUERY_TYPE_TIMESTAMP, queryIndex );
        commandList->ResolveQueryData( mQueryHeap, D3D12_QUERY_TYPE_TIMESTAMP, queryIndex, 1, mReadbackResource.GetResource(), queryIndex * sizeof( UINT64 ) );
    }

    UINT64 TimestampQueryHeap::GetTimestamp( UINT index )
    {
        assert( index < mTimestampCount );
        UINT queryIndex = index + gSwapIndex * mTimestampCount;
        UINT64 result = reinterpret_cast< UINT64* >( mReadbackResource.Map() )[ queryIndex ];
        mReadbackResource.Unmap();
        return result;
    }

    float TimestampQueryHeap::GetTimeDifference( UINT startIndex, UINT stopIndex )
    {
        return ( static_cast< float >( GetTimestamp( stopIndex ) - GetTimestamp( startIndex ) ) / static_cast< float >( mTimestampFrequency ) ) * 1000.0f;
    }
}