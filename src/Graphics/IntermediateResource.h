#pragma once

#include "DescriptorHeap.h"

#include <d3d12.h>
#include <d3dx12.h>

namespace Graphics
{
    class IntermediateResource
    {
    private:
        ID3D12Resource* mResource;
        D3D12_HEAP_TYPE mHeapType;
        UINT mSize;
        BYTE* mMappedData;

    public:
        IntermediateResource();

        void Create( D3D12_HEAP_TYPE heapType, UINT size );
        void Destroy();

        void CopyBuffer( ID3D12GraphicsCommandList* commandList,
                         ID3D12Resource* defaultResource, D3D12_RESOURCE_STATES defaultState );
        void CopyTexture2D( ID3D12GraphicsCommandList* commandList,
                            ID3D12Resource* defaultResource, D3D12_RESOURCE_STATES defaultState,
                            D3D12_PLACED_SUBRESOURCE_FOOTPRINT& layout );

        BYTE* Map();
        void Unmap();

        ID3D12Resource* GetResource() const;
        const UINT GetSize() const;

    private:
        void CopyBuffer( ID3D12GraphicsCommandList* commandList,
                         ID3D12Resource* destination, ID3D12Resource* source,
                         D3D12_RESOURCE_STATES defaultState, D3D12_RESOURCE_STATES copyState );
        void CopyTexture2D( ID3D12GraphicsCommandList* commandList,
                            ID3D12Resource* destination, ID3D12Resource* source,
                            D3D12_RESOURCE_STATES defaultState, D3D12_RESOURCE_STATES copyState,
                            D3D12_PLACED_SUBRESOURCE_FOOTPRINT& layout );
    };
}