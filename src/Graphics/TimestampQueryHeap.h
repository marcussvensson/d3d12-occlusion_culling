#pragma once

#include "IntermediateResource.h"

#include <d3d12.h>

namespace Graphics
{
    class TimestampQueryHeap
    {
    private:
        ID3D12QueryHeap* mQueryHeap;
        IntermediateResource mReadbackResource;
        UINT mTimestampCount;
        UINT64 mTimestampFrequency;

    public:
        TimestampQueryHeap();

        void Create( UINT timestampCount );
        void Destroy();

        void SetTimestampQuery( ID3D12GraphicsCommandList* commandList, UINT index );

        UINT64 GetTimestamp( UINT index );
        float GetTimeDifference( UINT startIndex, UINT stopIndex );
    };
}