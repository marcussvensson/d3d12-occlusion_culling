#include "Timer.h"

namespace Graphics
{
    Timer::Timer()
    {
        mCountsPerSecond = 1.0f / static_cast< float >( SDL_GetPerformanceFrequency() );
        mPreviousTimestamp = SDL_GetPerformanceCounter();
        Tick();
    }

    void Timer::Tick()
    {
        Uint64 timestamp = SDL_GetPerformanceCounter();
        mDeltaTime = static_cast< float >( timestamp - mPreviousTimestamp ) * mCountsPerSecond;
        mFramesPerSecond = 1.0f / mDeltaTime;
        mPreviousTimestamp = timestamp;
    }

    const float Timer::GetDeltaTime() const
    {
        return mDeltaTime;
    }
    const float Timer::GetFramesPerSecond() const
    {
        return mFramesPerSecond;
    }
}