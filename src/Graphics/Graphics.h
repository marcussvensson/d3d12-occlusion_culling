#pragma once

#include "Input.h"

#include "CommandContext.h"
#include "DescriptorHeap.h"
#include "IntermediateResource.h"
#include "Model.h"
#include "Resource.h"
#include "Shader.h"
#include "TimestampQueryHeap.h"
#include "ThreadContext.h"

#include "Timer.h"
#include "Camera.h"

#include <d3d12.h>
#include <d3dx12.h>

namespace Graphics
{
    void StartUp( UINT width, UINT height, const char* windowTitle );
    void ShutDown();

    bool PollEvents();

    void BeginFrame( const FLOAT* clearColor );
    void ResumeFrame();
    void EndFrame();

    void Present();

    ID3D12RootSignature* CreateRootSignature( UINT parameterCount, CD3DX12_ROOT_PARAMETER* parameterArray, D3D12_ROOT_SIGNATURE_FLAGS flags = D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT );
    
    ID3D12CommandSignature* CreateCommandSignature( UINT argumentCount, D3D12_INDIRECT_ARGUMENT_DESC* argumentArray,
                                                    UINT argumentSize, ID3D12RootSignature* rootSignature );

    ID3D12PipelineState* CreatePipelineState( D3D12_GRAPHICS_PIPELINE_STATE_DESC stateDesc );
    ID3D12PipelineState* CreatePipelineState( D3D12_COMPUTE_PIPELINE_STATE_DESC stateDesc );
    
    DescriptorHandle CreateSampler( D3D12_FILTER filter, D3D12_TEXTURE_ADDRESS_MODE addressMode, DescriptorHeap* descriptorHeap );

    CommandContext* GetContext();
    D3D12_VIEWPORT GetViewport();
}