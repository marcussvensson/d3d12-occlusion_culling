#include "Graphics.h"
#include "GraphicsDefines.h"

#include <stdlib.h>
#include <SDL_syswm.h>
#include <SDL_ttf.h>
#include <vector>

namespace Graphics
{
    SDL_Window* gWindow;

    ID3D12Device* gDevice;

    CommandContext gGraphicsContext;

    IDXGISwapChain* gSwapChain;
    UINT8 gSwapIndex;

    DescriptorHeap gRenderTargetHeap;
    ID3D12Resource* gRenderTargets[ 2 ];
    CD3DX12_CPU_DESCRIPTOR_HANDLE gRenderTargetViews[ 2 ];

    DescriptorHeap gDepthTargetHeap;
    Texture2DResource gDepthTargets[ 2 ];

    D3D12_VIEWPORT gViewport;
    D3D12_RECT gScissorRect;


    void PrintHardwareOptions();

    void StartUp( UINT width, UINT height, const char* windowTitle )
    {
        if ( SDL_Init( SDL_INIT_EVERYTHING ) != 0 || TTF_Init() != 0 )
        {
            printf( "Failed to initialize SDL.\n" );
            return;
        }

        gWindow = SDL_CreateWindow( windowTitle, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                   width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE );

        SDL_SysWMinfo info;
        SDL_VERSION( &info.version );

        HWND handle;
        if ( SDL_GetWindowWMInfo( gWindow, &info ) )
        {
            handle = info.info.win.window;
        }
        else
        {
            printf( "Failed to get window handle.\n" );
            return;
        }

    #ifdef _DEBUG
        ID3D12Debug* debugController = nullptr;
        D3D12GetDebugInterface( IID_PPV_ARGS( &debugController ) );
        if ( debugController != nullptr )
        {
            debugController->EnableDebugLayer();
            debugController->Release();
        }
    #endif

        HR( D3D12CreateDevice( nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS( &gDevice ) ) );

        PrintHardwareOptions();

        gGraphicsContext.Create( D3D12_COMMAND_LIST_TYPE_DIRECT, D3D12_FENCE_FLAG_NONE );

        IDXGIFactory* factory;
        HR( CreateDXGIFactory1( IID_PPV_ARGS( &factory ) ) );

        DXGI_SWAP_CHAIN_DESC swapChainDesc;
        ZeroMemory( &swapChainDesc, sizeof( swapChainDesc ) );
        swapChainDesc.BufferCount = 2;
        swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
        swapChainDesc.BufferUsage = DXGI_USAGE_BACK_BUFFER | DXGI_USAGE_RENDER_TARGET_OUTPUT;
        swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
        swapChainDesc.OutputWindow = handle;
        swapChainDesc.SampleDesc.Count = 1;
        swapChainDesc.Windowed = TRUE;
        HR( factory->CreateSwapChain( gGraphicsContext.GetCommandQueue(), &swapChainDesc, &gSwapChain ) );
        gSwapIndex = 0;

        factory->Release();

        gRenderTargetHeap.Create( 2, D3D12_DESCRIPTOR_HEAP_TYPE_RTV, D3D12_DESCRIPTOR_HEAP_FLAG_NONE );
        HR( gSwapChain->GetBuffer( 0, IID_PPV_ARGS( &gRenderTargets[ 0 ] ) ) );
        HR( gSwapChain->GetBuffer( 1, IID_PPV_ARGS( &gRenderTargets[ 1 ] ) ) );
        gRenderTargetViews[ 0 ] = gRenderTargetHeap.CreateHandle().cpu;
        gRenderTargetViews[ 1 ] = gRenderTargetHeap.CreateHandle().cpu;
        gDevice->CreateRenderTargetView( gRenderTargets[ 0 ], nullptr, gRenderTargetViews[ 0 ] );
        gDevice->CreateRenderTargetView( gRenderTargets[ 1 ], nullptr, gRenderTargetViews[ 1 ] );

        gDepthTargetHeap.Create( 2, D3D12_DESCRIPTOR_HEAP_TYPE_DSV, D3D12_DESCRIPTOR_HEAP_FLAG_NONE );
        TEXTURE2D_RESOURCE_DESC depthTargetDesc;
        ZeroMemory( &depthTargetDesc, sizeof( depthTargetDesc ) );
        depthTargetDesc.DefaultEnable = TRUE;
        depthTargetDesc.DefaultState = D3D12_RESOURCE_STATE_DEPTH_WRITE;
        depthTargetDesc.DsvDescriptorHandle = gDepthTargetHeap.CreateHandle();
        gDepthTargets[ 0 ].Create( &depthTargetDesc, width, height, DXGI_FORMAT_D32_FLOAT );
        depthTargetDesc.DsvDescriptorHandle = gDepthTargetHeap.CreateHandle();
        gDepthTargets[ 1 ].Create( &depthTargetDesc, width, height, DXGI_FORMAT_D32_FLOAT );

        HR( gDevice->SetStablePowerState( TRUE ) );

        gViewport = { 0.0f, 0.0f, static_cast< float >( width ), static_cast< float >( height ), 0.0f, 1.0f };
        gScissorRect = { 0, 0, static_cast< long >( width ), static_cast< long >( height ) };
    }

    void ShutDown()
    {
        gDepthTargets[ 1 ].Destroy();
        gDepthTargets[ 0 ].Destroy();
        gDepthTargetHeap.Destroy();

        gRenderTargets[ 1 ]->Release();
        gRenderTargets[ 0 ]->Release();
        gRenderTargetHeap.Destroy();

        gGraphicsContext.Destroy();

        gSwapChain->Release();

        gDevice->Release();

        SDL_DestroyWindow( gWindow );
        TTF_Quit();
        SDL_Quit();
    }

    bool PollEvents()
    {
        Input::Reset();

        SDL_Event event;
        while ( SDL_PollEvent( &event ) )
        {
            switch ( event.type )
            {
                case SDL_QUIT:
                    return false;
            }
            Input::ProcessEvent( event );
        }

        return true;
    }

    void BeginFrame( const FLOAT* clearColor )
    {
        ID3D12GraphicsCommandList* commandList = gGraphicsContext.GetCommandList();

        commandList->ResourceBarrier(
            1,
            &CD3DX12_RESOURCE_BARRIER::Transition(
                gRenderTargets[ gSwapIndex ],
                D3D12_RESOURCE_STATE_PRESENT,
                D3D12_RESOURCE_STATE_RENDER_TARGET ) );

        ResumeFrame();

        commandList->ClearRenderTargetView(
            gRenderTargetViews[ gSwapIndex ], clearColor, 0, nullptr );
        commandList->ClearDepthStencilView(
            gDepthTargets[ gSwapIndex ].GetDsvDescriptorHandle()->cpu,
            D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, nullptr );
    }

    void ResumeFrame()
    {
        ID3D12GraphicsCommandList* commandList = gGraphicsContext.GetCommandList();
        commandList->RSSetViewports( 1, &gViewport );
        commandList->RSSetScissorRects( 1, &gScissorRect );
        commandList->OMSetRenderTargets(
            1, &gRenderTargetViews[ gSwapIndex ], false,
            &gDepthTargets[ gSwapIndex ].GetDsvDescriptorHandle()->cpu );
    }

    void EndFrame()
    {
        ID3D12GraphicsCommandList* commandList = gGraphicsContext.GetCommandList();

        commandList->ResourceBarrier(
            1,
            &CD3DX12_RESOURCE_BARRIER::Transition(
                gRenderTargets[ gSwapIndex ],
                D3D12_RESOURCE_STATE_RENDER_TARGET,
                D3D12_RESOURCE_STATE_PRESENT ) );
    }

    void Present()
    {
        HR( gSwapChain->Present( 0, 0 ) );

        gGraphicsContext.WaitForGpu();

        gSwapIndex = 1 - gSwapIndex;
    }

    ID3D12RootSignature* CreateRootSignature( UINT parameterCount, CD3DX12_ROOT_PARAMETER* parameterArray, D3D12_ROOT_SIGNATURE_FLAGS flags )
    {
        CD3DX12_ROOT_SIGNATURE_DESC rootSignatureDesc;
        rootSignatureDesc.Init( parameterCount, parameterArray, 0, nullptr, flags );

        ID3DBlob* signature;
        ID3DBlob* error;
        if ( FAILED( D3D12SerializeRootSignature( &rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error ) ) )
        {
            if ( error )
                LOG( ( char* )error->GetBufferPointer() );
            else
                LOG( "D3D12SerializeRootSignature failed." );
        }

        ID3D12RootSignature* rootSignature;
        HR( gDevice->CreateRootSignature( 0, signature->GetBufferPointer(), signature->GetBufferSize(), __uuidof( ID3D12RootSignature ), ( void** )&rootSignature ) );

        if ( signature )
            signature->Release();
        if ( error )
            error->Release();

        return rootSignature;
    }

    ID3D12CommandSignature* CreateCommandSignature( UINT argumentCount, D3D12_INDIRECT_ARGUMENT_DESC* argumentArray,
                                                    UINT argumentSize, ID3D12RootSignature* rootSignature )
    {
        ID3D12CommandSignature* commandSignature;

        D3D12_COMMAND_SIGNATURE_DESC commandSignatureDesc;
        ZeroMemory( &commandSignatureDesc, sizeof( commandSignatureDesc ) );
        commandSignatureDesc.pArgumentDescs = argumentArray;
        commandSignatureDesc.NumArgumentDescs = argumentCount;
        commandSignatureDesc.ByteStride = argumentSize;
        HR( gDevice->CreateCommandSignature( &commandSignatureDesc, rootSignature, IID_PPV_ARGS( &commandSignature ) ) );
        
        return commandSignature;
    }

    ID3D12PipelineState* CreatePipelineState( D3D12_GRAPHICS_PIPELINE_STATE_DESC stateDesc )
    {
        ID3D12PipelineState* pipelineState;
        HR( gDevice->CreateGraphicsPipelineState( &stateDesc, IID_PPV_ARGS( &pipelineState ) ) );
        return pipelineState;
    }

    ID3D12PipelineState* CreatePipelineState( D3D12_COMPUTE_PIPELINE_STATE_DESC stateDesc )
    {
        ID3D12PipelineState* pipelineState;
        HR( gDevice->CreateComputePipelineState( &stateDesc, IID_PPV_ARGS( &pipelineState ) ) );
        return pipelineState;
    }

    DescriptorHandle CreateSampler( D3D12_FILTER filter, D3D12_TEXTURE_ADDRESS_MODE addressMode, DescriptorHeap* descriptorHeap )
    {
        DescriptorHandle descriptorHandle = descriptorHeap->CreateHandle();

        D3D12_SAMPLER_DESC samplerDesc;
        ZeroMemory( &samplerDesc, sizeof( samplerDesc ) );
        samplerDesc.Filter = filter;
        samplerDesc.AddressU = addressMode;
        samplerDesc.AddressV = addressMode;
        samplerDesc.AddressW = addressMode;
        gDevice->CreateSampler( &samplerDesc, descriptorHandle.cpu );

        return descriptorHandle;
    }

    void PrintHardwareOptions()
    {
        D3D12_FEATURE_DATA_D3D12_OPTIONS options;
        if ( FAILED( gDevice->CheckFeatureSupport( D3D12_FEATURE_D3D12_OPTIONS, &options, sizeof( D3D12_FEATURE_DATA_D3D12_OPTIONS ) ) ) )
        {
            LOG( "CheckFeatureSupport failed.\n" );
            return;
        }

        LOG( "-------------------------------- HARDWARE OPTIONS --------------------------------\n" );

        switch ( options.ConservativeRasterizationTier )
        {
            case D3D12_CONSERVATIVE_RASTERIZATION_TIER_NOT_SUPPORTED:
                LOG( "ConservativeRasterizationTier: D3D12_CONSERVATIVE_RASTERIZATION_NOT_SUPPORTED\n" );
                break;
            case D3D12_CONSERVATIVE_RASTERIZATION_TIER_1:
                LOG( "ConservativeRasterizationTier: D3D12_CONSERVATIVE_RASTERIZATION_TIER_1\n" );
                break;
            case D3D12_CONSERVATIVE_RASTERIZATION_TIER_2:
                LOG( "ConservativeRasterizationTier: D3D12_CONSERVATIVE_RASTERIZATION_TIER_2\n" );
                break;
            case D3D12_CONSERVATIVE_RASTERIZATION_TIER_3:
                LOG( "ConservativeRasterizationTier: D3D12_CONSERVATIVE_RASTERIZATION_TIER_3\n" );
                break;
        }

        if ( options.CrossAdapterRowMajorTextureSupported )
            LOG( "CrossAdapterRowMajorTextureSupported: TRUE\n" );
        else
            LOG( "CrossAdapterRowMajorTextureSupported: FALSE\n" );

        switch ( options.CrossNodeSharingTier )
        {
            case D3D12_CROSS_NODE_SHARING_TIER_NOT_SUPPORTED:
                LOG( "CrossNodeSharingTier: D3D12_CROSS_NODE_SHARING_NOT_SUPPORTED\n" );
                break;
            case D3D12_CROSS_NODE_SHARING_TIER_1_EMULATED:
                LOG( "CrossNodeSharingTier: D3D12_CROSS_NODE_SHARING_TIER_1_EMULATED\n" );
                break;
            case D3D12_CROSS_NODE_SHARING_TIER_1:
                LOG( "CrossNodeSharingTier: D3D12_CROSS_NODE_SHARING_TIER_1\n" );
                break;
            case D3D12_CROSS_NODE_SHARING_TIER_2:
                LOG( "CrossNodeSharingTier: D3D12_CROSS_NODE_SHARING_TIER_2\n" );
                break;
        }

        if ( options.DoublePrecisionFloatShaderOps )
            LOG( "DoublePrecisionFloatShaderOps: TRUE\n" );
        else
            LOG( "DoublePrecisionFloatShaderOps: FALSE\n" );

        LOG( "MaxGPUVirtualAddressBitsPerResource: %d\n", options.MaxGPUVirtualAddressBitsPerResource );

        switch ( options.MinPrecisionSupport )
        {
            case D3D12_SHADER_MIN_PRECISION_SUPPORT_NONE:
                LOG( "MinPrecisionSupport: D3D12_SHADER_MIN_PRECISION_NONE\n" );
                break;
            case D3D12_SHADER_MIN_PRECISION_SUPPORT_10_BIT:
                LOG( "MinPrecisionSupport: D3D12_SHADER_MIN_PRECISION_10_BIT\n" );
                break;
            case D3D12_SHADER_MIN_PRECISION_SUPPORT_16_BIT:
                LOG( "MinPrecisionSupport: D3D12_SHADER_MIN_PRECISION_16_BIT\n" );
                break;
        }

        if ( options.OutputMergerLogicOp )
            LOG( "OutputMergerLogicOp: TRUE\n" );
        else
            LOG( "OutputMergerLogicOp: FALSE\n" );

        if ( options.PSSpecifiedStencilRefSupported )
            LOG( "PSSpecifiedStencilRefSupported: TRUE\n" );
        else
            LOG( "PSSpecifiedStencilRefSupported: FALSE\n" );

        switch ( options.ResourceBindingTier )
        {
            case D3D12_RESOURCE_BINDING_TIER_1:
                LOG( "ResourceBindingTier: D3D12_RESOURCE_BINDING_TIER_1\n" );
                break;
            case D3D12_RESOURCE_BINDING_TIER_2:
                LOG( "ResourceBindingTier: D3D12_RESOURCE_BINDING_TIER_2\n" );
                break;
            case D3D12_RESOURCE_BINDING_TIER_3:
                LOG( "ResourceBindingTier: D3D12_RESOURCE_BINDING_TIER_3\n" );
                break;
        }

        switch ( options.ResourceHeapTier )
        {
            case D3D12_RESOURCE_HEAP_TIER_1:
                LOG( "ResourceHeapTier: D3D12_RESOURCE_HEAP_TIER_1\n" );
                break;
            case D3D12_RESOURCE_HEAP_TIER_2:
                LOG( "ResourceHeapTier: D3D12_RESOURCE_HEAP_TIER_2\n" );
                break;
        }

        if ( options.ROVsSupported )
            LOG( "ROVsSupported: TRUE\n" );
        else
            LOG( "ROVsSupported: FALSE\n" );

        if ( options.StandardSwizzle64KBSupported )
            LOG( "StandardSwizzle64KBSupported: TRUE\n" );
        else
            LOG( "StandardSwizzle64KBSupported: FALSE\n" );

        switch ( options.TiledResourcesTier )
        {
            case D3D12_TILED_RESOURCES_TIER_NOT_SUPPORTED:
                LOG( "TiledResourcesTier: D3D12_TILED_RESOURCES_NOT_SUPPORTED\n" );
                break;
            case D3D12_TILED_RESOURCES_TIER_1:
                LOG( "TiledResourcesTier: D3D12_TILED_RESOURCES_TIER_1\n" );
                break;
            case D3D12_TILED_RESOURCES_TIER_2:
                LOG( "TiledResourcesTier: D3D12_TILED_RESOURCES_TIER_2\n" );
                break;
            case D3D12_TILED_RESOURCES_TIER_3:
                LOG( "TiledResourcesTier: D3D12_TILED_RESOURCES_TIER_3\n" );
                break;
        }

        if ( options.TypedUAVLoadAdditionalFormats )
            LOG( "TypedUAVLoadAdditionalFormats: TRUE\n" );
        else
            LOG( "TypedUAVLoadAdditionalFormats: FALSE\n" );

        if ( options.VPAndRTArrayIndexFromAnyShaderFeedingRasterizerSupportedWithoutGSEmulation )
            LOG( "VPAndRTArrayIndexFromAnyShaderFeedingRasterizerSupportedWithoutGSEmulation: TRUE\n" );
        else
            LOG( "VPAndRTArrayIndexFromAnyShaderFeedingRasterizerSupportedWithoutGSEmulation: FALSE\n" );

        LOG( "----------------------------------------------------------------------------------\n" );
    }

    CommandContext* GetContext()
    {
        return &gGraphicsContext;
    }

    D3D12_VIEWPORT GetViewport()
    {
        return gViewport;
    }
}