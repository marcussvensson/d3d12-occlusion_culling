#include "Camera.h"

#include <d3d12.h>

namespace Graphics
{
    Camera::Camera()
        : mPosition( DirectX::XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f ) ),
          mRight( DirectX::XMVectorSet( 1.0f, 0.0f, 0.0f, 0.0f ) ),
          mUp( DirectX::XMVectorSet( 0.0f, 1.0f, 0.0f, 0.0f ) ),
          mLook( DirectX::XMVectorSet( 0.0f, 0.0f, 1.0f, 0.0f ) )
    {
    }

    void Camera::Strafe( float d )
    {
        mPosition = DirectX::XMVectorMultiplyAdd( DirectX::XMVectorReplicate( d ), mRight, mPosition );
    }

    void Camera::Walk( float d )
    {
        mPosition = DirectX::XMVectorMultiplyAdd( DirectX::XMVectorReplicate( d ), mLook, mPosition );
    }

    void Camera::Pitch( float angle )
    {
        DirectX::XMVECTOR R = DirectX::XMQuaternionRotationAxis( mRight, angle );
        mUp = DirectX::XMVector3Rotate( mUp, R );
        mLook = DirectX::XMVector3Rotate( mLook, R );
    }

    void Camera::Yaw( float angle )
    {
        DirectX::XMVECTOR R = DirectX::XMQuaternionRotationAxis( DirectX::XMVectorSet( 0.0f, 1.0f, 0.0f, 0.0f ), angle );
        mRight = DirectX::XMVector3Rotate( mRight, R );
        mUp = DirectX::XMVector3Rotate( mUp, R );
        mLook = DirectX::XMVector3Rotate( mLook, R );
    }

    void Camera::UpdateViewMatrix()
    {
        DirectX::XMVECTOR target = DirectX::XMVectorAdd( mPosition, mLook );
        mView = DirectX::XMMatrixLookAtRH( mPosition, target, mUp );
    }

    void Camera::ExtractFrustumPlanes( DirectX::XMFLOAT4 planes[ 6 ] )
    {
        DirectX::XMFLOAT4X4 M;
        DirectX::XMStoreFloat4x4( &M, GetViewProjection() );

        // Left
        planes[ 0 ].x = M( 0, 3 ) + M( 0, 0 );
        planes[ 0 ].y = M( 1, 3 ) + M( 1, 0 );
        planes[ 0 ].z = M( 2, 3 ) + M( 2, 0 );
        planes[ 0 ].w = M( 3, 3 ) + M( 3, 0 );

        // Right
        planes[ 1 ].x = M( 0, 3 ) - M( 0, 0 );
        planes[ 1 ].y = M( 1, 3 ) - M( 1, 0 );
        planes[ 1 ].z = M( 2, 3 ) - M( 2, 0 );
        planes[ 1 ].w = M( 3, 3 ) - M( 3, 0 );

        // Bottom
        planes[ 2 ].x = M( 0, 3 ) + M( 0, 1 );
        planes[ 2 ].y = M( 1, 3 ) + M( 1, 1 );
        planes[ 2 ].z = M( 2, 3 ) + M( 2, 1 );
        planes[ 2 ].w = M( 3, 3 ) + M( 3, 1 );

        // Top
        planes[ 3 ].x = M( 0, 3 ) - M( 0, 1 );
        planes[ 3 ].y = M( 1, 3 ) - M( 1, 1 );
        planes[ 3 ].z = M( 2, 3 ) - M( 2, 1 );
        planes[ 3 ].w = M( 3, 3 ) - M( 3, 1 );

        // Near
        planes[ 4 ].x = M( 0, 2 );
        planes[ 4 ].y = M( 1, 2 );
        planes[ 4 ].z = M( 2, 2 );
        planes[ 4 ].w = M( 3, 2 );

        // Far
        planes[ 5 ].x = M( 0, 3 ) - M( 0, 2 );
        planes[ 5 ].y = M( 1, 3 ) - M( 1, 2 );
        planes[ 5 ].z = M( 2, 3 ) - M( 2, 2 );
        planes[ 5 ].w = M( 3, 3 ) - M( 3, 2 );

        // Normalize the planes
        for ( UINT i = 0; i < 6; i++ )
        {
            DirectX::XMStoreFloat4( &planes[ i ], DirectX::XMPlaneNormalize( DirectX::XMLoadFloat4( &planes[ i ] ) ) );
        }
    }

    void Camera::SetPosition( DirectX::XMFLOAT3 position )
    {
        mPosition = XMLoadFloat3( &position );
    }

    void Camera::SetPerspective( float fovY, float aspectRatio, float nearZ, float farZ )
    {
        mProjection = DirectX::XMMatrixPerspectiveFovRH( fovY, aspectRatio, nearZ, farZ );
    }

    const DirectX::XMMATRIX Camera::GetView()
    {
        return mView;
    }

    const DirectX::XMMATRIX Camera::GetProjection()
    {
        return mProjection;
    }

    const DirectX::XMMATRIX Camera::GetViewProjection()
    {
        return DirectX::XMMatrixMultiply( mView, mProjection );
    }

    const DirectX::XMFLOAT3 Camera::GetPosition()
    {
        DirectX::XMFLOAT3 position;
        DirectX::XMStoreFloat3( &position, mPosition );
        return position;
    }

    const DirectX::XMFLOAT3 Camera::GetRight()
    {
        DirectX::XMFLOAT3 right;
        DirectX::XMStoreFloat3( &right, mRight );
        return right;
    }

    const DirectX::XMFLOAT3 Camera::GetUp()
    {
        DirectX::XMFLOAT3 up;
        DirectX::XMStoreFloat3( &up, mUp );
        return up;
    }

    const DirectX::XMFLOAT3 Camera::GetLook()
    {
        DirectX::XMFLOAT3 look;
        DirectX::XMStoreFloat3( &look, mLook );
        return look;
    }
}