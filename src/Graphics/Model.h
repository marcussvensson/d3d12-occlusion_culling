#pragma once

#include "GraphicsDefines.h"

#include <d3d12.h>
#include <d3dx12.h>

namespace Graphics
{
    extern ID3D12Device* gDevice;

    template< class Vertex >
    class Model
    {
    private:
        ID3D12Resource* mVertexBuffer;
        ID3D12Resource* mVertexBufferUpload;
        ID3D12Resource* mIndexBuffer;
        ID3D12Resource* mIndexBufferUpload;

        D3D12_VERTEX_BUFFER_VIEW mVertexBufferView;
        D3D12_INDEX_BUFFER_VIEW mIndexBufferView;

        UINT mIndexCount;

    public:
        void Create( ID3D12GraphicsCommandList* commandList,
                     size_t vertexCount, const Vertex* vertexArray,
                     size_t indexCount, const Index* indexArray );
        void Destroy();

        void Use( ID3D12GraphicsCommandList* commandList );
        void Draw( ID3D12GraphicsCommandList* commandList );

        const UINT GetIndexCount() const;

    private:
        const DXGI_FORMAT GetIndexBufferFormat();
    };

    template< class Vertex >
    void Model< Vertex >::Create( ID3D12GraphicsCommandList* commandList,
                                  size_t vertexCount, const Vertex* vertexArray,
                                  size_t indexCount, const Index* indexArray )
    {
        CD3DX12_HEAP_PROPERTIES defaultHeapProperties( D3D12_HEAP_TYPE_DEFAULT );
        CD3DX12_HEAP_PROPERTIES uploadHeapProperties( D3D12_HEAP_TYPE_UPLOAD );

        UINT vertexBufferSize = static_cast< UINT >( sizeof( Vertex ) * vertexCount );
        CD3DX12_RESOURCE_DESC vertexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer( vertexBufferSize );
        HR( gDevice->CreateCommittedResource(
            &defaultHeapProperties,
            D3D12_HEAP_FLAG_NONE,
            &vertexBufferDesc,
            D3D12_RESOURCE_STATE_COPY_DEST,
            nullptr,
            IID_PPV_ARGS( &mVertexBuffer ) ) );
        HR( gDevice->CreateCommittedResource(
            &uploadHeapProperties,
            D3D12_HEAP_FLAG_NONE,
            &vertexBufferDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS( &mVertexBufferUpload ) ) );

        D3D12_SUBRESOURCE_DATA vertexData;
        ZeroMemory( &vertexData, sizeof( vertexData ) );
        vertexData.pData = reinterpret_cast< const BYTE* >( vertexArray );
        vertexData.RowPitch = vertexBufferSize;
        vertexData.SlicePitch = vertexData.RowPitch;

        UpdateSubresources( commandList, mVertexBuffer, mVertexBufferUpload, 0, 0, 1, &vertexData );

        CD3DX12_RESOURCE_BARRIER vertexBufferResourceBarrier =
            CD3DX12_RESOURCE_BARRIER::Transition( mVertexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER );
        commandList->ResourceBarrier( 1, &vertexBufferResourceBarrier );

        mVertexBufferView.BufferLocation = mVertexBuffer->GetGPUVirtualAddress();
        mVertexBufferView.StrideInBytes = sizeof( Vertex );
        mVertexBufferView.SizeInBytes = vertexBufferSize;

        mIndexCount = static_cast< UINT >( indexCount );
        UINT indexBufferSize = static_cast< UINT >( sizeof( Index ) ) * mIndexCount;
        CD3DX12_RESOURCE_DESC indexBufferDesc = CD3DX12_RESOURCE_DESC::Buffer( indexBufferSize );
        HR( gDevice->CreateCommittedResource(
            &defaultHeapProperties,
            D3D12_HEAP_FLAG_NONE,
            &indexBufferDesc,
            D3D12_RESOURCE_STATE_COPY_DEST,
            nullptr,
            IID_PPV_ARGS( &mIndexBuffer ) ) );
        HR( gDevice->CreateCommittedResource(
            &uploadHeapProperties,
            D3D12_HEAP_FLAG_NONE,
            &indexBufferDesc,
            D3D12_RESOURCE_STATE_GENERIC_READ,
            nullptr,
            IID_PPV_ARGS( &mIndexBufferUpload ) ) );

        D3D12_SUBRESOURCE_DATA indexData;
        ZeroMemory( &indexData, sizeof( indexData ) );
        indexData.pData = reinterpret_cast< const BYTE* >( indexArray );
        indexData.RowPitch = indexBufferSize;
        indexData.SlicePitch = indexData.RowPitch;

        UpdateSubresources( commandList, mIndexBuffer, mIndexBufferUpload, 0, 0, 1, &indexData );

        CD3DX12_RESOURCE_BARRIER indexBufferResourceBarrier =
            CD3DX12_RESOURCE_BARRIER::Transition( mIndexBuffer, D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE );
        commandList->ResourceBarrier( 1, &indexBufferResourceBarrier );

        mIndexBufferView.BufferLocation = mIndexBuffer->GetGPUVirtualAddress();
        mIndexBufferView.Format = GetIndexBufferFormat();
        mIndexBufferView.SizeInBytes = indexBufferSize;
    }

    template< class Vertex >
    void Model< Vertex >::Destroy()
    {
        mVertexBufferUpload->Release();
        mVertexBuffer->Release();
        mIndexBufferUpload->Release();
        mIndexBuffer->Release();
    }

    template< class Vertex >
    void Model< Vertex >::Use( ID3D12GraphicsCommandList* commandList )
    {
        commandList->IASetPrimitiveTopology( D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST );
        commandList->IASetVertexBuffers( 0, 1, &mVertexBufferView );
        commandList->IASetIndexBuffer( &mIndexBufferView );
    }

    template< class Vertex >
    void Model< Vertex >::Draw( ID3D12GraphicsCommandList* commandList )
    {
        Use( commandList );
        commandList->DrawIndexedInstanced( mIndexCount, 1, 0, 0, 0 );
    }

    template< class Vertex >
    const UINT Model< Vertex >::GetIndexCount() const
    {
        return mIndexCount;
    }

    template< class Vertex >
    const DXGI_FORMAT Model< Vertex >::GetIndexBufferFormat()
    {
        DXGI_FORMAT indexBufferFormat;
        switch ( sizeof( Index ) )
        {
            case 1:
                indexBufferFormat = DXGI_FORMAT_R8_UINT;
                break;
            case 2:
                indexBufferFormat = DXGI_FORMAT_R16_UINT;
                break;
            default:
                indexBufferFormat = DXGI_FORMAT_R32_UINT;
                break;
        }
        return indexBufferFormat;
    }
}