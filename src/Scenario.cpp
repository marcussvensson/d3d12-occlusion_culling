#include "Scenario.h"
#include "Graphics/Graphics.h"

#include <d3dx12.h>
#include <DirectXColors.h>
#include <vector>
#include <random>
#include <math.h>

using namespace Graphics;

void Scenario::Create()
{
    CreatePipelines();

    mCbvSrvUavHeap.Create( 7, D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE );
    mSamplerHeap.Create( 1, D3D12_DESCRIPTOR_HEAP_TYPE_SAMPLER, D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE );

    ID3D12GraphicsCommandList* commandList = Graphics::GetContext()->GetCommandList();
    LoadResources( commandList );
    Graphics::GetContext()->CloseCommandList();
    Graphics::GetContext()->ExecuteCommandList();
    Graphics::GetContext()->WaitForGpu();
    
    mTimestampQueryHeap.Create( 16 );

    float fovY = 70.0f * DirectX::XM_PI / 180.0f;
    float aspectRatio = Graphics::GetViewport().Width / Graphics::GetViewport().Height;
    mCamera.SetPerspective( fovY, aspectRatio, 0.01f, 100.0f );
    mCamera.UpdateViewMatrix();

    mComputeContext.Create( D3D12_COMMAND_LIST_TYPE_DIRECT, D3D12_FENCE_FLAG_NONE );
    mComputeContext.CloseCommandList();
}

void Scenario::CreatePipelines()
{
    // Create root signatures
    CD3DX12_ROOT_PARAMETER worldRootParameters[ 1 ];
    worldRootParameters[ 0 ].InitAsConstantBufferView( 0, 0, D3D12_SHADER_VISIBILITY_VERTEX );
    mWorldRootSignature = Graphics::CreateRootSignature( _countof( worldRootParameters ), worldRootParameters );

    CD3DX12_DESCRIPTOR_RANGE viewCbvRanges[ 1 ];
    viewCbvRanges[ 0 ].Init( D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0 );
    CD3DX12_DESCRIPTOR_RANGE viewSrvRanges[ 1 ];
    viewSrvRanges[ 0 ].Init( D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 1, 0 );
    CD3DX12_DESCRIPTOR_RANGE viewSamplerRanges[ 1 ];
    viewSamplerRanges[ 0 ].Init( D3D12_DESCRIPTOR_RANGE_TYPE_SAMPLER, 1, 0 );
    CD3DX12_ROOT_PARAMETER viewParameters[ 3 ];
    viewParameters[ 0 ].InitAsDescriptorTable( _countof( viewCbvRanges ), viewCbvRanges, D3D12_SHADER_VISIBILITY_VERTEX );
    viewParameters[ 1 ].InitAsDescriptorTable( _countof( viewSrvRanges ), viewSrvRanges, D3D12_SHADER_VISIBILITY_PIXEL );
    viewParameters[ 2 ].InitAsDescriptorTable( _countof( viewSamplerRanges ), viewSamplerRanges, D3D12_SHADER_VISIBILITY_PIXEL );
    mViewRootSignature = Graphics::CreateRootSignature( _countof( viewParameters ), viewParameters );

    CD3DX12_DESCRIPTOR_RANGE occlusionCullingCbvRanges[ 1 ];
    occlusionCullingCbvRanges[ 0 ].Init( D3D12_DESCRIPTOR_RANGE_TYPE_CBV, 1, 0 );
    CD3DX12_DESCRIPTOR_RANGE occlusionCullingSrvUavRanges[ 2 ];
    occlusionCullingSrvUavRanges[ 0 ].Init( D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 2, 0 );
    occlusionCullingSrvUavRanges[ 1 ].Init( D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0 );
    CD3DX12_ROOT_PARAMETER occlusionCullingParameters[ 2 ];
    occlusionCullingParameters[ 0 ].InitAsDescriptorTable( _countof( occlusionCullingCbvRanges ), occlusionCullingCbvRanges );
    occlusionCullingParameters[ 1 ].InitAsDescriptorTable( _countof( occlusionCullingSrvUavRanges ), occlusionCullingSrvUavRanges );
    mOcclusionCullingRootSignature = Graphics::CreateRootSignature( _countof( occlusionCullingParameters ), occlusionCullingParameters );

    // Create command signature
    D3D12_INDIRECT_ARGUMENT_DESC argumentArray[ 2 ];
    ZeroMemory( &argumentArray, sizeof( argumentArray ) );
    argumentArray[ 0 ].Type = D3D12_INDIRECT_ARGUMENT_TYPE_CONSTANT_BUFFER_VIEW;
    argumentArray[ 1 ].Type = D3D12_INDIRECT_ARGUMENT_TYPE_DRAW_INDEXED;
    mCommandSignature = Graphics::CreateCommandSignature( _countof( argumentArray ), argumentArray, sizeof( IndirectCommand ), mWorldRootSignature );

    // Load shaders
    Shader worldVertexShader( L"res/shaders/WorldVertexShader.hlsl", "main", "vs_5_0" );
    Shader worldPixelShader( L"res/shaders/WorldPixelShader.hlsl", "main", "ps_5_0" );
    Shader viewVertexShader( L"res/shaders/ViewVertexShader.hlsl", "main", "vs_5_0" );
    Shader viewPixelShader( L"res/shaders/ViewPixelShader.hlsl", "main", "ps_5_0" );
    Shader occlusionCullingComputeShader( L"res/shaders/OcclusionCullingComputeShader.hlsl", "main", "cs_5_0" );

    // Create world pipeline state
    D3D12_GRAPHICS_PIPELINE_STATE_DESC graphicsStateDesc;
    ZeroMemory( &graphicsStateDesc, sizeof( graphicsStateDesc ) );
    graphicsStateDesc.InputLayout = worldVertexShader.GetInputLayout();
    graphicsStateDesc.pRootSignature = mWorldRootSignature;
    graphicsStateDesc.VS = worldVertexShader.GetShaderBytecode();
    graphicsStateDesc.PS = worldPixelShader.GetShaderBytecode();
    graphicsStateDesc.RasterizerState = CD3DX12_RASTERIZER_DESC( D3D12_DEFAULT );
    graphicsStateDesc.BlendState = CD3DX12_BLEND_DESC( D3D12_DEFAULT );
    graphicsStateDesc.DepthStencilState = CD3DX12_DEPTH_STENCIL_DESC( D3D12_DEFAULT );
    graphicsStateDesc.DepthStencilState.DepthEnable = TRUE;
    graphicsStateDesc.DepthStencilState.StencilEnable = FALSE;
    graphicsStateDesc.SampleMask = UINT_MAX;
    graphicsStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
    graphicsStateDesc.NumRenderTargets = 1;
    graphicsStateDesc.RTVFormats[ 0 ] = DXGI_FORMAT_R8G8B8A8_UNORM;
    graphicsStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT;
    graphicsStateDesc.SampleDesc.Count = 1;
    mWorldPipelineState = Graphics::CreatePipelineState( graphicsStateDesc );

    // Create view pipeline state
    graphicsStateDesc.InputLayout = viewVertexShader.GetInputLayout();
    graphicsStateDesc.pRootSignature = mViewRootSignature;
    graphicsStateDesc.VS = viewVertexShader.GetShaderBytecode();
    graphicsStateDesc.PS = viewPixelShader.GetShaderBytecode();
    graphicsStateDesc.BlendState.AlphaToCoverageEnable = TRUE;
    mViewPipelineState = Graphics::CreatePipelineState( graphicsStateDesc );

    // Create occlusion culling pipeline state
    D3D12_COMPUTE_PIPELINE_STATE_DESC computeStateDesc;
    ZeroMemory( &computeStateDesc, sizeof( computeStateDesc ) );
    computeStateDesc.pRootSignature = mOcclusionCullingRootSignature;
    computeStateDesc.CS = occlusionCullingComputeShader.GetShaderBytecode();
    mOcclusionCullingPipelineState = Graphics::CreatePipelineState( computeStateDesc );
}

void Scenario::LoadResources( ID3D12GraphicsCommandList* commandList )
{
    // Create view and occlusion culling constant buffers
    BUFFER_RESOURCE_DESC constantBufferDesc;
    ZeroMemory( &constantBufferDesc, sizeof( constantBufferDesc ) );
    constantBufferDesc.UploadEnable = TRUE;
    constantBufferDesc.CbvDescriptorHandle = mCbvSrvUavHeap.CreateHandle();
    mOcclusionCullingConstantBuffer.Create( &constantBufferDesc );

    // Map view and occlusion culling constant buffers
    mOcclusionCullingConstantBuffer.GetUpload()->Map();

    // Create cube model
    std::vector<WorldVertex> cubeVertices;
    cubeVertices.insert( cubeVertices.begin(), {
        { DirectX::XMFLOAT3( -0.5f, -0.5f, -0.5f ), DirectX::XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ) },
        { DirectX::XMFLOAT3( -0.5f, -0.5f,  0.5f ), DirectX::XMFLOAT4( 0.0f, 0.0f, 1.0f, 1.0f ) },
        { DirectX::XMFLOAT3( -0.5f,  0.5f, -0.5f ), DirectX::XMFLOAT4( 0.0f, 1.0f, 0.0f, 1.0f ) },
        { DirectX::XMFLOAT3( -0.5f,  0.5f,  0.5f ), DirectX::XMFLOAT4( 0.0f, 1.0f, 1.0f, 1.0f ) },
        { DirectX::XMFLOAT3( 0.5f, -0.5f, -0.5f ), DirectX::XMFLOAT4( 1.0f, 0.0f, 0.0f, 1.0f ) },
        { DirectX::XMFLOAT3( 0.5f, -0.5f,  0.5f ), DirectX::XMFLOAT4( 1.0f, 0.0f, 1.0f, 1.0f ) },
        { DirectX::XMFLOAT3( 0.5f,  0.5f, -0.5f ), DirectX::XMFLOAT4( 1.0f, 1.0f, 0.0f, 1.0f ) },
        { DirectX::XMFLOAT3( 0.5f,  0.5f,  0.5f ), DirectX::XMFLOAT4( 1.0f, 1.0f, 1.0f, 1.0f ) }
    } );
    std::vector<Index> cubeIndices;
    cubeIndices.insert( cubeIndices.begin(), {
        0, 2, 1,
        1, 2, 3,
        4, 5, 6,
        5, 7, 6,
        0, 1, 5,
        0, 5, 4,
        2, 6, 7,
        2, 7, 3,
        0, 4, 6,
        0, 6, 2,
        1, 3, 7,
        1, 7, 5,
    } );
    mCubeModel.Create( commandList,
                       cubeVertices.size(), &cubeVertices[ 0 ],
                       cubeIndices.size(), &cubeIndices[ 0 ] );
    mCubeCount = 1000;

    // Create cube constant buffer
    BUFFER_RESOURCE_DESC cubeConstantBufferDesc;
    ZeroMemory( &cubeConstantBufferDesc, sizeof( cubeConstantBufferDesc ) );
    cubeConstantBufferDesc.UploadEnable = TRUE;
    mCubeConstantBuffer.Create( &cubeConstantBufferDesc, static_cast< UINT >( mCubeCount ) );

    // Create AABB buffer
    BUFFER_RESOURCE_DESC aabbBufferDesc;
    ZeroMemory( &aabbBufferDesc, sizeof( aabbBufferDesc ) );
    aabbBufferDesc.DefaultEnable = TRUE;
    aabbBufferDesc.UploadEnable = TRUE;
    aabbBufferDesc.DefaultState = D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;
    aabbBufferDesc.SrvDescriptorHandle = mCbvSrvUavHeap.CreateHandle();
    mAabbBuffer.Create( &aabbBufferDesc, static_cast< UINT >( mCubeCount ) );

    // Create cube constant buffers, randomize positions and add AABBs to the AABB buffer
    std::random_device randomDevice;
    std::mt19937 generator( randomDevice() );
    std::uniform_real_distribution<float> distribution( -16.0f, 16.0f );
    WorldConstantBuffer* cubeConstantBufferArray = mCubeConstantBuffer.MapUpload();
    AABB* aabbBufferArray = mAabbBuffer.MapUpload();
    for ( size_t i = 0; i < mCubeCount; ++i )
    {
        float x = distribution( generator );
        float y = distribution( generator );
        float z = distribution( generator );
        DirectX::XMStoreFloat4x4( &cubeConstantBufferArray[ i ].gModel, DirectX::XMMatrixTranspose( DirectX::XMMatrixTranslation( x, y, z ) ) );

        aabbBufferArray[ i ].points[ 0 ] = DirectX::XMFLOAT3( x - 0.5f, y - 0.5f, z - 0.5f );
        aabbBufferArray[ i ].points[ 1 ] = DirectX::XMFLOAT3( x + 0.5f, y + 0.5f, z + 0.5f );
    }
    mAabbBuffer.GetUpload()->Unmap();
    mAabbBuffer.Write( commandList );

    // Create input command buffer
    BUFFER_RESOURCE_DESC inputCommandBufferDesc;
    ZeroMemory( &inputCommandBufferDesc, sizeof( inputCommandBufferDesc ) );
    inputCommandBufferDesc.DefaultEnable = TRUE;
    inputCommandBufferDesc.UploadEnable = TRUE;
    inputCommandBufferDesc.DefaultState = D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE;
    inputCommandBufferDesc.SrvDescriptorHandle = mCbvSrvUavHeap.CreateHandle();
    mInputCommandBuffer.Create( &inputCommandBufferDesc, static_cast< UINT >( mCubeCount ) );

    // Fill input command buffer with commands
    IndirectCommand* inputCommandArray = mInputCommandBuffer.MapUpload();
    D3D12_GPU_VIRTUAL_ADDRESS gpuAddress = mCubeConstantBuffer.GetUpload()->GetResource()->GetGPUVirtualAddress();
    for ( size_t i = 0; i < mCubeCount; ++i )
    {
        inputCommandArray[ i ].cbvAddress = gpuAddress;
        inputCommandArray[ i ].drawArguments.IndexCountPerInstance = mCubeModel.GetIndexCount();
        inputCommandArray[ i ].drawArguments.InstanceCount = 1;
        inputCommandArray[ i ].drawArguments.StartIndexLocation = 0;
        inputCommandArray[ i ].drawArguments.BaseVertexLocation = 0;
        inputCommandArray[ i ].drawArguments.StartInstanceLocation = 0;

        gpuAddress += sizeof( WorldConstantBuffer );
    }
    mInputCommandBuffer.GetUpload()->Unmap();
    mInputCommandBuffer.Write( commandList );

    // Create output command buffer
    BUFFER_RESOURCE_DESC outputCommandBufferDesc;
    ZeroMemory( &outputCommandBufferDesc, sizeof( outputCommandBufferDesc ) );
    outputCommandBufferDesc.DefaultEnable = TRUE;
    outputCommandBufferDesc.UploadEnable = TRUE;
    outputCommandBufferDesc.ReadbackEnable = TRUE;
    outputCommandBufferDesc.DefaultState = D3D12_RESOURCE_STATE_UNORDERED_ACCESS;
    outputCommandBufferDesc.UavDescriptorHandle = mCbvSrvUavHeap.CreateHandle();
    outputCommandBufferDesc.UavCounterEnable = TRUE;
    mOutputputCommandBuffer.Create( &outputCommandBufferDesc, static_cast< UINT >( mCubeCount ) );

    // Map output command buffer and zero memory
    ZeroMemory( mOutputputCommandBuffer.GetUpload()->Map(), mOutputputCommandBuffer.GetUpload()->GetSize() );
    mOutputputCommandBuffer.GetUpload()->Unmap();
    mOutputputCommandBuffer.Write( commandList );

    // Create quad model
    std::vector<ViewVertex> quadVertices;
    quadVertices.insert( quadVertices.begin(), {
        { DirectX::XMFLOAT3( 0.0f, 0.0f, 0.0f ), DirectX::XMFLOAT2( 0.0f, 1.0f ) },
        { DirectX::XMFLOAT3( 0.0f, 1.0f, 0.0f ), DirectX::XMFLOAT2( 0.0f, 0.0f ) },
        { DirectX::XMFLOAT3( 1.0f, 0.0f, 0.0f ), DirectX::XMFLOAT2( 1.0f, 1.0f ) },
        { DirectX::XMFLOAT3( 1.0f, 1.0f, 0.0f ), DirectX::XMFLOAT2( 1.0f, 0.0f ) }
    } );
    std::vector<Index> quadIndices;
    quadIndices.insert( quadIndices.begin(), {
        0, 1, 2,
        3, 2, 1
    } );
    mQuadModel.Create( commandList,
                       quadVertices.size(), &quadVertices[ 0 ],
                       quadIndices.size(), &quadIndices[ 0 ] );

    // Create text resource
    TEXTURE2D_RESOURCE_DESC textDesc;
    ZeroMemory( &textDesc, sizeof( textDesc ) );
    textDesc.DefaultEnable = TRUE;
    textDesc.UploadEnable = TRUE;
    textDesc.DefaultState = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE;
    textDesc.SrvDescriptorHandle = mCbvSrvUavHeap.CreateHandle();
    mTextResource.Create( &textDesc, 512, 256, DXGI_FORMAT_B8G8R8A8_UNORM );
    mTextResource.OpenFont( "res/fonts/barthowheel.ttf", 32, { 255, 255, 255, 0 } );

    // Map text resource and zero memory
    ZeroMemory( mTextResource.GetUpload()->Map(), mTextResource.GetUpload()->GetSize() );
    mTextResource.Write( commandList );

    // Create text constant buffer
    BUFFER_RESOURCE_DESC textConstantBufferDesc;
    ZeroMemory( &textConstantBufferDesc, sizeof( textConstantBufferDesc ) );
    textConstantBufferDesc.UploadEnable = TRUE;
    textConstantBufferDesc.CbvDescriptorHandle = mCbvSrvUavHeap.CreateHandle();
    mTextConstantBuffer.Create( &textConstantBufferDesc );

    // Map text constant buffer and copy text scale matrix
    DirectX::XMStoreFloat4x4( &mTextConstantBuffer.MapUpload()->gModel,
                              DirectX::XMMatrixTranspose(
                                  DirectX::XMMatrixMultiply(
                                      DirectX::XMMatrixTranslation( 0.02f, 0.02f, 0.0f ),
                                      mTextResource.GetScaleMatrix() ) ) );

    // Create linear sampler
    mPointSamplerDescriptorHandle = Graphics::CreateSampler( D3D12_FILTER_MIN_MAG_MIP_LINEAR , D3D12_TEXTURE_ADDRESS_MODE_WRAP, &mSamplerHeap );
}

void Scenario::Destroy()
{
    mTimestampQueryHeap.Destroy();

    mTextConstantBuffer.Destroy();
    mTextResource.Destroy();
    mQuadModel.Destroy();

    mOutputputCommandBuffer.Destroy();
    mInputCommandBuffer.Destroy();
    mAabbBuffer.Destroy();

    mCubeConstantBuffer.Destroy();
    mCubeModel.Destroy();

    mOcclusionCullingConstantBuffer.Destroy();

    mSamplerHeap.Destroy();
    mCbvSrvUavHeap.Destroy();

    mCommandSignature->Release();

    mOcclusionCullingPipelineState->Release();
    mViewPipelineState->Release();
    mWorldPipelineState->Release();

    mOcclusionCullingRootSignature->Release();
    mViewRootSignature->Release();
    mWorldRootSignature->Release();

    mComputeContext.Destroy();
}

void Scenario::Update( float dt )
{
    if ( Input::IsMouseButtonDown( SDL_BUTTON_RIGHT ) )
    {
        const Sint32* dmp = Input::GetDeltaMousePosition();
        mCamera.Yaw( -dmp[ 0 ] * dt * 0.5f );
        mCamera.Pitch( dmp[ 1 ] * dt * 0.5f );
    }

    const float speed = 5.0f;
    if ( Input::IsKeyDown( SDL_SCANCODE_W ) )
        mCamera.Walk( dt * speed );
    if ( Input::IsKeyDown( SDL_SCANCODE_S ) )
        mCamera.Walk( -dt * speed );
    if ( Input::IsKeyDown( SDL_SCANCODE_A ) )
        mCamera.Strafe( dt * speed );
    if ( Input::IsKeyDown( SDL_SCANCODE_D ) )
        mCamera.Strafe( -dt * speed );
    mCamera.UpdateViewMatrix();

    // Update world constant buffer
    for ( size_t i = 0; i < mCubeCount; ++i )
    {
        DirectX::XMStoreFloat4x4(
            &mCubeConstantBuffer.MapUpload()[ i ].gViewProjection,
            DirectX::XMMatrixTranspose( mCamera.GetViewProjection() ) );
    }

    // Update occlusion culling constant buffer
    mCamera.ExtractFrustumPlanes( mOcclusionCullingConstantBuffer.MapUpload()->gFrustumPlanes );
    mOcclusionCullingConstantBuffer.MapUpload()->gAabbCount = static_cast< UINT >( mCubeCount );
}

void Scenario::Draw()
{
    // Occlusion culling compute work
    mComputeContext.ResetCommandList( mOcclusionCullingPipelineState );
    ID3D12GraphicsCommandList* computeCommandList = mComputeContext.GetCommandList();

    computeCommandList->SetPipelineState( mOcclusionCullingPipelineState );
    computeCommandList->SetComputeRootSignature( mOcclusionCullingRootSignature );
    ID3D12DescriptorHeap* computeHeaps[] = { mCbvSrvUavHeap.GetHeap() };
    computeCommandList->SetDescriptorHeaps( _countof( computeHeaps ), computeHeaps );
    computeCommandList->SetComputeRootDescriptorTable( 0, mOcclusionCullingConstantBuffer.GetCbvDescriptorHandle()->gpu );
    computeCommandList->SetComputeRootDescriptorTable( 1, mAabbBuffer.GetSrvDescriptorHandle()->gpu );
    
    mOutputputCommandBuffer.Write( computeCommandList );
    mTimestampQueryHeap.SetTimestampQuery( computeCommandList, 0 );
    computeCommandList->Dispatch( static_cast< UINT >( ceilf( static_cast< float >( mCubeCount ) / 128.0f ) ), 1, 1 );
    mTimestampQueryHeap.SetTimestampQuery( computeCommandList, 1 );
    mOutputputCommandBuffer.Read( computeCommandList );

    mComputeContext.CloseCommandList();

    // World space rendering
    Graphics::GetContext()->ResetCommandList( mWorldPipelineState );
    ID3D12GraphicsCommandList* graphicsCommandList = Graphics::GetContext()->GetCommandList();
    Graphics::BeginFrame( DirectX::Colors::CornflowerBlue );

    graphicsCommandList->SetGraphicsRootSignature( mWorldRootSignature );
    ID3D12DescriptorHeap* worldHeaps[] = { mCbvSrvUavHeap.GetHeap() };
    graphicsCommandList->SetDescriptorHeaps( _countof( worldHeaps ), worldHeaps );
   
    mOutputputCommandBuffer.Transition( graphicsCommandList, D3D12_RESOURCE_STATE_INDIRECT_ARGUMENT );
    mCubeModel.Use( graphicsCommandList );
    mTimestampQueryHeap.SetTimestampQuery( graphicsCommandList, 2 );
    graphicsCommandList->ExecuteIndirect(
        mCommandSignature,
        static_cast< UINT >( mCubeCount ),
        mOutputputCommandBuffer.GetDefaultResource(),
        0,
        mOutputputCommandBuffer.GetDefaultResource(),
        mOutputputCommandBuffer.GetUpload()->GetSize() - 4 );
    mTimestampQueryHeap.SetTimestampQuery( graphicsCommandList, 3 );
    mOutputputCommandBuffer.Transition( graphicsCommandList, D3D12_RESOURCE_STATE_UNORDERED_ACCESS );

    // Execute world space GPU work
    Graphics::GetContext()->CloseCommandList();
    mComputeContext.ExecuteCommandList();
    Graphics::GetContext()->Wait( mComputeContext );
    Graphics::GetContext()->ExecuteCommandList();
    Graphics::GetContext()->WaitForGpu();

    // View space rendering
    Graphics::GetContext()->ResetCommandList( mViewPipelineState );
    Graphics::ResumeFrame();

    BYTE* mappedOutputCommandBuffer = mOutputputCommandBuffer.GetReadback()->Map();
    mappedOutputCommandBuffer += mOutputputCommandBuffer.GetReadback()->GetSize() - 4;
    UINT visibleCubeCount = *reinterpret_cast< UINT* >( mappedOutputCommandBuffer );
    mOutputputCommandBuffer.GetReadback()->Unmap();

    char buffer[ 256 ];
    sprintf_s(
        buffer,
        "NUMBER OF VISIBLE CUBES: %d/%zd\nFRUSTUM CULLING TIME: %f ms\nINDIRECT DRAWING TIME: %f ms",
        visibleCubeCount, mCubeCount,
        mTimestampQueryHeap.GetTimeDifference( 0, 1 ),
        mTimestampQueryHeap.GetTimeDifference( 2, 3 ) );
    mTextResource.UpdateText( buffer );
    mTextResource.Write( graphicsCommandList );

    graphicsCommandList->SetGraphicsRootSignature( mViewRootSignature );
    ID3D12DescriptorHeap* viewHeaps[] = { mCbvSrvUavHeap.GetHeap(), mSamplerHeap.GetHeap() };
    graphicsCommandList->SetDescriptorHeaps( _countof( viewHeaps ), viewHeaps );
    graphicsCommandList->SetGraphicsRootDescriptorTable( 0, mTextConstantBuffer.GetCbvDescriptorHandle()->gpu );
    graphicsCommandList->SetGraphicsRootDescriptorTable( 1, mTextResource.GetSrvDescriptorHandle()->gpu );
    graphicsCommandList->SetGraphicsRootDescriptorTable( 2, mPointSamplerDescriptorHandle.gpu );

    mQuadModel.Draw( graphicsCommandList );

    // Execute view space GPU work
    Graphics::EndFrame();
    Graphics::GetContext()->CloseCommandList();
    Graphics::GetContext()->ExecuteCommandList();
    Graphics::Present();
}