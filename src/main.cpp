#include "Graphics/Graphics.h"
#include "Scenario.h"

#undef main
int main(int argc, const char** argv)
{
    Graphics::StartUp( 1024, 768, "d3d12-occlusion_culling" );

    Scenario scenario;
    scenario.Create();

    Graphics::Timer timer;
    while ( Graphics::PollEvents() )
    {
        timer.Tick();

        if ( Input::IsKeyPressed( SDL_SCANCODE_ESCAPE ) )
            break;

        scenario.Update( timer.GetDeltaTime() );
        scenario.Draw();
    }

    scenario.Destroy();

    Graphics::ShutDown();

	return 0;
}