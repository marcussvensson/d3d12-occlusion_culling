#pragma once

#include "Graphics/Graphics.h"
#include "ShaderData.h"

#include <d3d12.h>
#include <vector>

class Scenario
{
private:
    Graphics::CommandContext mComputeContext;

    ID3D12RootSignature* mWorldRootSignature;
    ID3D12PipelineState* mWorldPipelineState;
    ID3D12RootSignature* mViewRootSignature;
    ID3D12PipelineState* mViewPipelineState;
    ID3D12RootSignature* mOcclusionCullingRootSignature;
    ID3D12PipelineState* mOcclusionCullingPipelineState;

    ID3D12CommandSignature* mCommandSignature;

    Graphics::DescriptorHeap mCbvSrvUavHeap;
    Graphics::DescriptorHeap mSamplerHeap;

    Graphics::BufferResource<OcclusionCullingBufferData> mOcclusionCullingConstantBuffer;

    size_t mCubeCount;
    Graphics::Model<WorldVertex> mCubeModel;
    Graphics::BufferResource<WorldConstantBuffer> mCubeConstantBuffer;

    Graphics::BufferResource<AABB> mAabbBuffer;
    Graphics::BufferResource<IndirectCommand> mInputCommandBuffer;
    Graphics::BufferResource<IndirectCommand> mOutputputCommandBuffer;

    Graphics::Model<ViewVertex> mQuadModel;
    Graphics::TextResource mTextResource;
    Graphics::BufferResource<ViewConstantBuffer> mTextConstantBuffer;
    Graphics::DescriptorHandle mPointSamplerDescriptorHandle;

    Graphics::TimestampQueryHeap mTimestampQueryHeap;

    Graphics::Camera mCamera;

public:
    void Create();
    void Destroy();

    void Update( float dt );
    void Draw();

private:
    void CreatePipelines();
    void LoadResources( ID3D12GraphicsCommandList* commandList );
};