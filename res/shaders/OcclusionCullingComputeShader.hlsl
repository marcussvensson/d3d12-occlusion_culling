#define BLOCK_SIZE_X 128

cbuffer OcclusionCullingConstantBuffer : register( b0 )
{
    float4 gFrustumPlanes[ 6 ];
    uint gAabbCount;
};

struct AABB
{
    float3 points[ 2 ];
};

struct IndirectCommand
{
    uint4 data0;
    uint4 data1;
};

StructuredBuffer<AABB> gAabbBuffer : register( t0 );
StructuredBuffer<IndirectCommand> gInputCommandBuffer : register( t1 );
AppendStructuredBuffer<IndirectCommand> gOutputCommandBuffer : register( u0 );

bool IsAabbInsideFrustum( AABB aabb )
{
    for ( int i = 0; i < 6; ++i )
    {
        int px = int( gFrustumPlanes[ i ].x > 0.0f );
        int py = int( gFrustumPlanes[ i ].y > 0.0f );
        int pz = int( gFrustumPlanes[ i ].z > 0.0f );

        float dp =
            gFrustumPlanes[ i ].x * aabb.points[ px ].x +
            gFrustumPlanes[ i ].y * aabb.points[ py ].y +
            gFrustumPlanes[ i ].z * aabb.points[ pz ].z;

        if ( dp < -gFrustumPlanes[ i ].w )
        {
            return false;
        }
    }

    return true;
}

[ numthreads( BLOCK_SIZE_X, 1, 1 ) ]
void main( uint3 groupId : SV_GroupID, uint groupIndex : SV_GroupIndex )
{
    uint index = ( groupId.x * BLOCK_SIZE_X ) + groupIndex;
    if ( index < gAabbCount && IsAabbInsideFrustum( gAabbBuffer[ index ] ) )
    {
        gOutputCommandBuffer.Append( gInputCommandBuffer[ index ] );
    }
}
