Texture2D<float4> gDiffuseTexture : register( t0 );

SamplerState gSampler : register( s0 );

struct PixelShaderInput
{
    float4 position : SV_POSITION;
    float2 uv : UV0;
};

float4 main(PixelShaderInput input) : SV_TARGET
{
    return gDiffuseTexture.Sample( gSampler, input.uv );
}
