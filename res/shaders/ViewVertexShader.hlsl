cbuffer ViewSpaceConstantBuffer : register(b0)
{
	matrix gModel;
};

struct VertexShaderInput
{
	float3 position : POSITION;
	float2 uv : UV0;
};

struct PixelShaderInput
{
	float4 position : SV_POSITION;
	float2 uv : UV0;
};

PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;

    float4 position = float4( input.position, 1.0f );
    position.y = 1.0f - position.y;
    position = mul( position, gModel );
    position.x = position.x * 2.0f - 1.0f;
    position.y = 1.0f - position.y * 2.0f;
	output.position = position;

    output.uv = input.uv;

	return output;
}
