cbuffer WorldSpaceConstantBuffer : register(b0)
{
    matrix gModel;
    matrix gViewProjection;
};

struct VertexShaderInput
{
	float3 position : POSITION;
	float4 color : COLOR0;
};

struct PixelShaderInput
{
	float4 position : SV_POSITION;
	float4 color : COLOR0;
};

PixelShaderInput main(VertexShaderInput input)
{
	PixelShaderInput output;

    float4 position = float4( input.position, 1.0f );
    position = mul( position, gModel );
    position = mul( position, gViewProjection );
	output.position = position;

	output.color = input.color;

	return output;
}
